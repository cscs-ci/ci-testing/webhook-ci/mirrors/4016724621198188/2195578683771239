package util

import (
    "context"
    "io/ioutil"
    "net/http"
    "sync"
    "time"

    "github.com/hashicorp/go-retryablehttp"
    "github.com/xanzy/go-gitlab"
    "cscs.ch/cicd-ext-mw/logging"
)

var onceGitlab sync.Once
var onceGitlabJobToken sync.Once
var glserver *gitlab.Client
var glserverJobToken *gitlab.Client

var gitlab_url string
var gitlab_token string

func SetupGitlab(url string, token string) {
    gitlab_url = url
    gitlab_token = token
}

type gitlabContextKey int
const RetryAlso400Key gitlabContextKey = 400

func GitlabRequestMustSucceed(ctx context.Context) gitlab.RequestOptionFunc {
    return gitlab.WithContext(context.WithValue(ctx, RetryAlso400Key, true))
}
// This Retry policy allows to set for a gitlab request to also retry errorcode=4XX, e.g.
// gitlab.Jobs.GetJob(pid, jid, gitlab.WithContext(context.WithValue(parentCtx, gitlab.RetryAlso400Key, true)))
func gitlabRetry(ctx context.Context, resp *http.Response, err error) (bool, error) {
    logger := logging.Get()
    if ctx.Err() != nil {
        return false, ctx.Err()
    }
    if err != nil {
        logger.Warn().Msgf("gitlabRetry has an error=%v. Will retry", err)
        return true, err
    }
    reqUrl := resp.Request.URL
    if resp.StatusCode == 429 || resp.StatusCode >= 500 {
        if resp.Body != nil {
            if bodyData, err := ioutil.ReadAll(resp.Body); err != nil {
                logger.Error().Err(err).Msgf("gitlabRetry reqUrl=%v got a response with Statuscode=%v and could not read  Body. Error while reading body=%v", reqUrl, resp.StatusCode, err)
            } else {
                logger.Warn().Msgf("gitlabRetry reqUrl=%v got a response with Statuscode=%v body=%v. Will retry", reqUrl, resp.StatusCode, string(bodyData))
            }
        } else {
            logger.Warn().Msgf("gitlabRetry reqUrl=%v got a response with Statuscode=%v and resp.Body==nil", reqUrl, resp.StatusCode)
        }
        return true, nil
    }
    if val, ok := ctx.Value(RetryAlso400Key).(bool); ok && val && resp.StatusCode >= 400 {
        if resp.Body != nil {
            if bodyData, err := ioutil.ReadAll(resp.Body); err != nil {
                logger.Warn().Msgf("gitlabRetry reqUrl=%v got a response with Statucode=%v and reading body failed with err=%v and we found in the context to also retry errorcode 4XX. Will retry", reqUrl, resp.StatusCode, err)
            } else {
                logger.Warn().Msgf("gitlabRetry reqUrl=%v got a response with Statucode=%v and body=%v and we found in the context to also retry errorcode 4XX. Will retry", reqUrl, resp.StatusCode, string(bodyData))
            }
        } else {
            logger.Warn().Msgf("gitlabRetry reqUrl=%v got a response with Statucode=%v and body=nil and we found in the context to also retry errorcode 4XX. Will retry", reqUrl, resp.StatusCode)
        }
        return true, nil
    }
    return false, nil
}


// make sure to call SetupGitlab before a
func GetGitlab() *gitlab.Client {
    logger := logging.Get()
    onceGitlab.Do(func() {
        var err error
        glserver, err = gitlab.NewClient(gitlab_token,
            gitlab.WithBaseURL(gitlab_url),
            gitlab.WithCustomLogger(logger),
            gitlab.WithCustomRetry(gitlabRetry),
            gitlab.WithCustomRetryMax(30),
            gitlab.WithCustomRetryWaitMinMax(1*time.Second, 300*time.Second),
            gitlab.WithCustomBackoff(retryablehttp.DefaultBackoff),
        )
        if err != nil {
            logger.Error().Err(err).Msgf("Error connecting to gitlab with url=%v", gitlab_url)
            panic(err)
        }
        // test if url and token can be used
        _, _, err = glserver.Version.GetVersion()
        if err != nil {
            logger.Error().Err(err).Msg("Error contacting the version endpoint of gitlab. Is the token correct?")
            panic(err)
        }
    })

    return glserver
}

func GetGitlabJobToken() *gitlab.Client {
    logger := logging.Get()
    onceGitlabJobToken.Do(func() {
        var err error
        glserverJobToken, err = gitlab.NewJobClient("",
            gitlab.WithBaseURL(gitlab_url),
            gitlab.WithCustomLogger(logger),
            gitlab.WithCustomRetry(gitlabRetry),
            gitlab.WithCustomRetryMax(30),
            gitlab.WithCustomRetryWaitMinMax(1*time.Second, 300*time.Second),
            gitlab.WithCustomBackoff(retryablehttp.DefaultBackoff),
        )
        if err != nil {
            logger.Error().Err(err).Msgf("Error connecting to gitlab with url=%v", gitlab_url)
            panic(err)
        }
    })

    return glserverJobToken
}
