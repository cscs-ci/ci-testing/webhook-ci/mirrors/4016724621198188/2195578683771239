package util

import (
    "database/sql"
    "errors"
    "fmt"
    "math/rand"
    "strings"
    "sync"

    _ "github.com/go-sql-driver/mysql"
    "github.com/snapcore/snapd/randutil"
    "cscs.ch/cicd-ext-mw/logging"
)

var dbpath string = ""

type AccessLevelType int
const (
    NoAccess      AccessLevelType = 0
    ManagerAccess AccessLevelType = 10
    AdminAccess   AccessLevelType = 20
    OwnerAccess   AccessLevelType = 30
)

type Credential struct {
    GlProjectID int
    JobID int
    Username string
    Password string
    System string
}

type StatusUpdateNeeded struct {
    PipelineID int64
    TargetURL string
    SHA string
    Name string
}

type ScheduleDataNoVars struct {
    ScheduleID int64
    CronSchedule string
    Ref string
}
type ScheduleData struct {
    ScheduleDataNoVars
    Variables map[string]string
}

type RepositoryAccess struct {
    RepoID int64
    AccessLevel AccessLevelType
    Claim string
}

type Variable struct {
    Key string
    Value string
    Secret bool
}

var once sync.Once
type DB struct {
    db *sql.DB
}
var db DB

func SetDBPath(p string) {
    dbpath = p
}

// make sure that you first call SetDBPath(...)
func GetDb() DB {
    logger := logging.Get()
    once.Do(func() {
        //dbpath = fmt.Sprintf("%v?_foreign_keys=true&mode=rw", dbpath)
        logger.Debug().Msgf("Using database at path %v", dbpath)
        var err error
        db.db, err = sql.Open("mysql", dbpath)
        if err != nil {
            logger.Error().Err(err).Msg("Error opening database")
            panic(err)
        }
        // sanity check that opened database makes sense:
        if db.GetRepoUrl(123456) == "" {
            logger.Error().Msg("Could not find repository with ID=123456. This indicates that an incorrect database was opened")
            panic("Error opening DB")
        }
    })

    return db
}
func GetDbRawConnection() *sql.DB {
    return GetDb().db
}

func (db DB) FindRepo(url string) int64 {
    var ret int64
    row := db.db.QueryRow("select repository_id from repository where repo_url=?", url)
    if err := row.Scan(&ret); err != nil {
        if err != sql.ErrNoRows {
            logging.Warnf("Failed querying the database looking in FindRepo. url=%v Err=%v", url, err)
        }
        return 0
    }
    return ret
}

func (db DB) GetRepoUrl(repo_id int64) string {
    var ret string
    db.getRepositoryAttribute(repo_id, "repo_url", &ret)
    return ret
}

func (db DB) GetRepositoryName(repo_id int64) string {
    var ret string
    db.getRepositoryAttribute(repo_id, "name", &ret)
    return ret
}
func (db DB) UpdateRepositoryName(repo_id int64, new_name string) bool {
    return db.setRepositoryAttribute(repo_id, "name", new_name)
}

func (db DB) GetWebhookSecret(repo_id int64) string {
    var ret string
    db.getRepositoryAttribute(repo_id, "webhook_secret", &ret)
    return ret
}
func (db DB) UpdateWebhookSecret(repo_id int64, new_secret string) bool {
    return db.setRepositoryAttribute(repo_id, "webhook_secret", new_secret)
}

func (db DB) GetMirrorWebhookSecret(pipeline_id int64) string {
    var ret string
    db.getPipelineAttribute(pipeline_id, "webhook_secret", &ret)
    return ret
}

func (db DB) IsPrivateRepo(repo_id int64) bool {
    var ret bool
    db.getRepositoryAttribute(repo_id, "private", &ret)
    return ret
}
func (db DB) UpdatePrivateRepoFlag(repo_id int64, is_private bool) bool {
    return db.setRepositoryAttribute(repo_id, "private", is_private)
}

func (db DB) GetNotificationToken(repo_id int64) string {
    var ret string
    db.getRepositoryAttribute(repo_id, "notification_token", &ret)
    return ret
}
func (db DB) UpdateNotificationToken(repo_id int64, new_token string) bool {
    return db.setRepositoryAttribute(repo_id, "notification_token", new_token)
}

func (db DB) GetF7tClientId(repo_id int64) string {
    var ret string
    db.getRepositoryAttribute(repo_id, "f7t_client_id", &ret)
    return ret
}
func (db DB) UpdateF7tClientId(repo_id int64, new_client_id string) bool {
    return db.setRepositoryAttribute(repo_id, "f7t_client_id", new_client_id)
}

func (db DB) GetF7tClientSecret(repo_id int64) string {
    var ret string
    db.getRepositoryAttribute(repo_id, "f7t_client_secret", &ret)
    return ret
}
func (db DB) UpdateF7tClientSecret(repo_id int64, new_client_secret string) bool {
    return db.setRepositoryAttribute(repo_id, "f7t_client_secret", new_client_secret)
}

func (db DB) GetF7tSlurmAccount(repo_id int64) string {
    var ret string
    db.getRepositoryAttribute(repo_id, "f7t_slurm_account", &ret)
    return ret
}
func (db DB) UpdateF7tSlurmAccount(repo_id int64, new_slurm_account string) bool {
    return db.setRepositoryAttribute(repo_id, "f7t_slurm_account", new_slurm_account)
}

func (db DB) GetPipelines(repo_id int64) []int64 {
    rows, err := db.db.Query("select pipeline_id from pipeline where repository_id = ?", repo_id)
    ret := rows_to_slice[int64](rows, err)
    return ret
}

func (db DB) GetPipelineId(name string, repo_id int64) int64 {
    var ret int64
    row := db.db.QueryRow("select pipeline_id from pipeline where repository_id=? and name=?", repo_id, name)
    if err := row.Scan(&ret); err != nil {
        logging.Warnf("Could not find pipleine_id for name=%v and repo_id=%v. Err=%v", name, repo_id, err)
        return -1
    }
    return ret
}

func (db DB) GetRepositoryId(pipeline_id int64) int64 {
    var ret int64
    db.getPipelineAttribute(pipeline_id, "repository_id", &ret)
    return ret
}

func (db DB) GetPipelineName(pipeline_id int64) string {
    var ret string
    db.getPipelineAttribute(pipeline_id, "name", &ret)
    return ret
}
func (db DB) UpdatePipelineName(pipeline_id int64, new_name string) bool {
    return db.setPipelineAttribute(pipeline_id, "name", new_name)
}

func (db DB) GetMirrorUrl(pipeline_id int64) string {
    var ret string
    db.getPipelineAttribute(pipeline_id, "repo_url", &ret)
    return ret
}
func (db DB) UpdateMirrorUrl(pipeline_id int64, new_url string) bool {
    return db.setPipelineAttribute(pipeline_id, "repo_url", new_url)
}

func (db DB) GetCIEntrypoint(pipeline_id int64) string {
    var ret string
    db.getPipelineAttribute(pipeline_id, "ci_entrypoint", &ret)
    return ret
}
func (db DB) UpdateCIEntrypoint(pipeline_id int64, new_entrypoint string) bool {
    return db.setPipelineAttribute(pipeline_id, "ci_entrypoint", new_entrypoint)
}

func (db DB) GetTriggerPR(pipeline_id int64) bool {
    var ret bool
    db.getPipelineAttribute(pipeline_id, "trigger_pr", &ret)
    return ret
}
func (db DB) UpdateTriggerPRFlag(pipeline_id int64, new_flag bool) bool {
    return db.setPipelineAttribute(pipeline_id, "trigger_pr", new_flag)
}

func (db DB) GetRepositoryVariables(repository_id int64) []Variable {
    var ret []Variable
    variables_rows, err := db.db.Query("select `key`,`value`, `secret` from repository_variable where repository_id=?", repository_id)
    if err != nil {
        logging.Error(err, "Failed getting variables rows for repository_variable")
        return ret
    }
    defer variables_rows.Close()
    for variables_rows.Next() {
        var key, value string
        var secret bool
        if err := variables_rows.Scan(&key, &value, &secret); err != nil {
            logging.Error(err, "Failed scanning row for repository_variable key/value/secret")
            return ret
        }
        ret = append(ret, Variable{Key: key, Value: value, Secret: secret})
    }
    return ret
}
func (db DB) AddRepositoryVariable(repository_id int64, key, value string, secret bool) bool {
    res, err := db.db.Exec("insert into repository_variable (repository_id, `key`, value, secret) values (?,?,?,?)", repository_id, key, value, secret)
    if err != nil {
        logging.Errorf(err, "Failed adding repository_variable with repository_id=%v, key=%v, value=%v, secret=%v, err=%v", repository_id, key, value, secret, err)
        return false
    }
    if num_changed, err := res.RowsAffected() ; err != nil {
        logging.Errorf(err, "Failed adding repository_variable with repository_id=%v, key=%v, value=%v, secret=%v, err=%v", repository_id, key, value, secret, err)
        return false
    } else {
        return num_changed==1
    }
}
func (db DB) DeleteRepositoryVariable(repository_id int64, key string) bool {
    res, err := db.db.Exec("delete from repository_variable where repository_id=? and `key`=?", repository_id, key)
    if err != nil {
        logging.Errorf(err, "Failed deleting repository_variable with repository_id=%v, key=%v, err=%v", repository_id, key, err)
        return false
    }
    if num_changed, err := res.RowsAffected() ; err != nil {
        logging.Errorf(err, "Failed deleting repository_variable with repository_id=%v, key=%v, err=%v", repository_id, key, err)
        return false
    } else {
        return num_changed==1
    }
}

func (db DB) GetPipelineVariables(pipeline_id int64) []Variable {
    var ret []Variable
    variables_rows, err := db.db.Query("select `key`,`value`, `secret` from pipeline_variable where pipeline_id=?", pipeline_id)
    if err != nil {
        logging.Error(err, "Failed getting variables rows for pipeline_variable")
        return ret
    }
    defer variables_rows.Close()
    for variables_rows.Next() {
        var key, value string
        var secret bool
        if err := variables_rows.Scan(&key, &value, &secret); err != nil {
            logging.Error(err, "Failed scanning row for pipeline_variable key/value/secret")
            return ret
        }
        ret = append(ret, Variable{Key: key, Value: value, Secret: secret})
    }
    return ret
}
func (db DB) AddPipelineVariable(pipeline_id int64, key, value string, secret bool) bool {
    res, err := db.db.Exec("insert into pipeline_variable (pipeline_id, `key`, value, secret) values (?,?,?,?)", pipeline_id, key, value, secret)
    if err != nil {
        logging.Errorf(err, "Failed adding pipeline_variable with pipeline_id=%v, key=%v, value=%v, secret=%v, err=%v", pipeline_id, key, value, secret, err)
        return false
    }
    if num_changed, err := res.RowsAffected() ; err != nil {
        logging.Errorf(err, "Failed adding pipeline_variable with pipeline_id=%v, key=%v, value=%v, secret=%v, err=%v", pipeline_id, key, value, secret, err)
        return false
    } else {
        return num_changed==1
    }
}
func (db DB) DeletePipelineVariable(pipeline_id int64, key string) bool {
    res, err := db.db.Exec("delete from pipeline_variable where pipeline_id=? and `key`=?", pipeline_id, key)
    if err != nil {
        logging.Errorf(err, "Failed deleting pipeline_variable with pipeline_id=%v, key=%v, err=%v", pipeline_id, key, err)
        return false
    }
    if num_changed, err := res.RowsAffected() ; err != nil {
        logging.Errorf(err, "Failed deleting pipeline_variable with pipeline_id=%v, key=%v, err=%v", pipeline_id, key, err)
        return false
    } else {
        return num_changed==1
    }
}

func (db DB) GetAllPipelineSchedules() []ScheduleDataNoVars {
    rows, err := db.db.Query("select schedule_id,cron_schedule,ref from pipeline_schedule")
    defer rows.Close()
    if err != nil {
        logging.Error(err, "Failed querying all cron schedules")
        return []ScheduleDataNoVars{}
    }
    var ret[]ScheduleDataNoVars
    for rows.Next() {
        var thisSchedule ScheduleDataNoVars
        if err := rows.Scan(&thisSchedule.ScheduleID, &thisSchedule.CronSchedule, &thisSchedule.Ref); err != nil {
            logging.Error(err, "Failed scanning row for sid and cron_schedule")
            return []ScheduleDataNoVars{}
        }
        ret = append(ret, thisSchedule)
    }
    return ret
}

func (db DB) GetPipelineSchedule(schedule_id int64) (ScheduleData, int64) {
    var data ScheduleData
    var pipeline_id int64
    data.ScheduleID = schedule_id
    row := db.db.QueryRow("select cron_schedule,ref,pipeline_id from pipeline_schedule where schedule_id=?", schedule_id)
    if err := row.Scan(&data.CronSchedule, &data.Ref, &pipeline_id); err != nil {
        logging.Warnf("Could not find schedule data for schedule_id=%v. Err=%v", schedule_id, err)
    }
    data.Variables = db.GetPipelineScheduleVariables(schedule_id)
    return data, pipeline_id
}

func (db DB) GetPipelineScheduleVariables(schedule_id int64) map[string]string {
    ret := make(map[string]string)
    variables_rows, err := db.db.Query("select `key`,`value` from schedule_variable where schedule_id=?", schedule_id)
    if err != nil {
        logging.Error(err, "Failed getting variables rows for cron schedule")
        return ret
    }
    defer variables_rows.Close()
    for variables_rows.Next() {
        var key, value string
        if err := variables_rows.Scan(&key, &value); err != nil {
            logging.Error(err, "Failed scanning row for schedule variables key/value")
            return ret
        }
        ret[key] = value
    }
    return ret
}

func (db DB) GetPipelineSchedules(pipeline_id int64) []ScheduleData {
    rows, err := db.db.Query("select schedule_id,cron_schedule,ref from pipeline_schedule where pipeline_id=?", pipeline_id)
    defer rows.Close()
    if err != nil {
        logging.Error(err, "Failed querying cron schedule")
        return []ScheduleData{}
    }
    var ret []ScheduleData
    for rows.Next() {
        var thisSchedule ScheduleData
        if err := rows.Scan(&thisSchedule.ScheduleID, &thisSchedule.CronSchedule, &thisSchedule.Ref); err != nil {
            logging.Error(err, "Failed scanning row for sid and cron_schedule")
            return []ScheduleData{}
        }
        thisSchedule.Variables = db.GetPipelineScheduleVariables(thisSchedule.ScheduleID)
        ret = append(ret, thisSchedule)
    }
    return ret
}

func (db DB) UpdatePipelineSchedule(schedule_id int64, cron_schedule, ref string, variables map[string]string) bool {
    if result, err := db.db.Exec("update pipeline_schedule set cron_schedule=?, ref=? where schedule_id=?", cron_schedule, ref, schedule_id) ; err != nil {
        logging.Warnf("Could not update pipeline_schedule with schedule_id=%v to cron_schedule=%v. err=%v", schedule_id, cron_schedule, err)
        return false
    } else {
        if num_changed, err := result.RowsAffected() ; err != nil {
            logging.Warnf("Error calling RowsAffected(). err=%v", err)
            return false
        } else {
            db.SetPipelineScheduleVariables(schedule_id, variables)
            return num_changed == 1
        }
    }
}
func (db DB) AddPipelineSchedule(pipeline_id int64, cron_schedule, ref string, variables map[string]string) bool {
    if result, err := db.db.Exec("insert into pipeline_schedule (pipeline_id,cron_schedule,ref) values (?,?,?)", pipeline_id, cron_schedule, ref); err != nil {
        logging.Warnf("Could not insert new cron schedule for pipeline_id=%v. err=%v", pipeline_id, err)
        return false
    } else {
        if new_schedule_id, err := result.LastInsertId() ; err != nil {
            logging.Warnf("Error calling LastInsertId(). err=%v", err)
            return false
        } else {
            db.SetPipelineScheduleVariables(new_schedule_id, variables)
            return new_schedule_id >= 1
        }
    }
}

func (db DB) SetPipelineScheduleVariables(schedule_id int64, variables map[string]string) {
    if _, err := db.db.Exec("delete from schedule_variable where schedule_id=?", schedule_id); err != nil {
        logging.Warnf("Could not delete schedule variables for schedule_id=%v. err=%v", schedule_id, err)
        return
    }
    if len(variables) > 0 {
        query := "insert into schedule_variable (`schedule_id`,`key`,`value`) values "
        num_vars := 3
        params := make([]interface{}, len(variables)*num_vars)
        pos := 0
        for k,v := range(variables) {
            params[pos] = schedule_id
            params[pos+1] = k
            params[pos+2] = v
            query += "(?,?,?),"
            pos += num_vars
        }
        query = query[:len(query)-1] // drop last comma
        if _, err := db.db.Exec(query, params...); err != nil {
            logging.Warnf("Failed inserting sechulde variables for schedule_id=%v. err=%v", schedule_id, err)
            return
        }
    }
}

func (db DB) DeletePipelineSchedule(schedule_id int64) bool {
    res, err := db.db.Exec("delete from pipeline_schedule where schedule_id=?", schedule_id)
    if err != nil {
        logging.Errorf(err, "Failed deleting pipeline_schedule with id=%v, err=%v", schedule_id, err)
        return false
    }
    if num_changed, err := res.RowsAffected() ; err != nil {
        logging.Errorf(err, "Failed deleting pipeline_schedule with id=%v, err=%v", schedule_id, err)
        return false
    } else {
        return num_changed==1
    }
}


func (db DB) GetBranchesDefault(repository_id int64) []string {
    rows, err := db.db.Query("select branch from branch_default where repository_id = ?", repository_id)
    return rows_to_slice[string](rows, err)
}
func (db DB) GetBranchesPipeline(pipeline_id int64) []string {
    rows, err := db.db.Query("select branch from branch where pipeline_id = ?", pipeline_id)
    return rows_to_slice[string](rows, err)
}

func (db DB) GetGitUsersDefault(repository_id int64) []string {
    rows, err := db.db.Query("select username from git_user_default where repository_id = ?", repository_id)
    return rows_to_slice[string](rows, err)
}
func (db DB) GetGitUsersPipeline(pipeline_id int64) []string {
    rows, err := db.db.Query("select username from git_user where pipeline_id = ?", pipeline_id)
    return rows_to_slice[string](rows, err)
}

func (db DB) ReplaceBranchesDefault(repository_id int64, new_branches_list []string) error {
    return db.replaceMany("branch_default", "repository_id", "branch", repository_id, new_branches_list)
}
func (db DB) ReplaceBranchesPipeline(pipeline_id int64, new_branches_list []string) error {
    return db.replaceMany("branch", "pipeline_id", "branch", pipeline_id, new_branches_list)
}
func (db DB) ReplaceGitUsersDefault(repository_id int64, new_users_list []string) error {
    return db.replaceMany("git_user_default", "repository_id", "username", repository_id, new_users_list)
}
func (db DB) ReplaceGitUsersPipeline(pipeline_id int64, new_users_list []string) error {
    return db.replaceMany("git_user", "pipeline_id", "username", pipeline_id, new_users_list)
}
// helper function to replace trusted users / ci-enabled branches - probably not useful for other table manipulations
func (db DB) replaceMany(table string, id_column string, column_name string, repo_or_pipeline_id int64, new_list []string) error {
    // internal function - we trust the values table and id_column and column_name
    var err error
    // nosemgrep: gosec.G202-1
    if _, err = db.db.Exec(fmt.Sprintf("delete from %v where %v=?", table, id_column), repo_or_pipeline_id); err != nil {
        logging.Errorf(err, "Failed delete sql query to remove all %v for %v=%v, err=%v", table, id_column, repo_or_pipeline_id, err)
        return err
    }
    for _, val := range new_list {
        // nosemgrep: gosec.G202-1
        if _, err = db.db.Exec(fmt.Sprintf("insert into %v (%v,%v) values (?,?)", table, id_column, column_name), repo_or_pipeline_id, val) ; err != nil {
            logging.Errorf(err, "Failed adding %v=%v to %v for %v=%v", column_name, val, table, id_column, repo_or_pipeline_id)
            return err
        }
    }
    return nil
}

func (db DB) GetRunnerAllowance(repository_id int64, runner_id string) (bool, int) {
    // runner_id can be either the real ID of the runner in gitlab
    // or a tag name (e.g. daint-container)
    var max_num_nodes int
    row := db.db.QueryRow("select max_num_nodes from repository_allowance where repository_id=? and runner_id=?", repository_id, runner_id)
    if err := row.Scan(&max_num_nodes); err != nil {
        return false,0
    }
    return true,max_num_nodes
}

func (db DB) AddProject(name string) (int64, error) {
    new_project_id := db.getRandomId("project")
    _, err := db.db.Exec("insert into project (project_id, name) VALUES (?,?)", new_project_id, name)
    if err != nil {
        logging.Errorf(err, "Failed adding project, project_id=%v, name=%v, err=%v", new_project_id, name, err)
        return 0, err
    }
    return new_project_id, nil
}

func (db DB) AddRepository(project_id int64, repo_url, name, owner, webhook_secret string, private bool) (int64, error) {
    new_repo_id := db.getRandomId("repository")
    _, err := db.db.Exec("insert into repository (repository_id, project_id, repo_url, name, webhook_secret, private) VALUES (?,?,?,?,?,?)", new_repo_id, project_id, repo_url, name, webhook_secret, private)
    if err != nil {
        logging.Errorf(err, "Failed adding repository, repository_id=%v, project_id=%v, repo_url=%v, name=%v, private=%v, err=%v", new_repo_id, project_id, repo_url, name, private, err)
        return 0, err
    }
    if err := db.SetRepoOwner(new_repo_id, owner); err != nil {
        logging.Error(err, "Failed setting repository owner")
    }
    return new_repo_id, nil
}

func (db DB) AddPipeline(repository_id int64, name string) (int64, error) {
    var highest_pipeline_id int64
    row := db.db.QueryRow("select pipeline_id from pipeline where repository_id=? order by pipeline_id desc limit 1", repository_id)
    var new_pipeline_id int64
    if err := row.Scan(&highest_pipeline_id); err != nil {
        logging.Errorf(err, "Could not find a single pipeline for repository_id=%v", repository_id)
        new_pipeline_id = db.getRandomId("pipeline")
    } else {
        new_pipeline_id = db.getNextId("pipeline", highest_pipeline_id)
    }
    mirror_webhook_secret, err := randutil.CryptoToken(16)
    if err != nil {
        logging.Errorf(err, "Failed adding pipeline, repository_id=%v, name=%v, err=%v", repository_id, name, err)
        return 0, err
    }
    _, err = db.db.Exec("insert into pipeline (pipeline_id, repository_id, webhook_secret, name, ci_entrypoint) VALUES (?,?,?,?,'')", new_pipeline_id, repository_id, mirror_webhook_secret, name)
    if err != nil {
        logging.Errorf(err, "Failed adding pipeline, repository_id=%v, name=%v, err=%v", repository_id, name, err)
        return 0, err
    }
    return new_pipeline_id, nil
}

func (db DB) DeletePipeline(pipeline_id int64) (bool, error) {
    res, err := db.db.Exec("delete from pipeline where pipeline_id=?", pipeline_id)
    if err != nil {
        logging.Errorf(err, "Failed deleting pipeline with id=%v, err=%v", pipeline_id, err)
        return false, err
    }
    if num_changed, err := res.RowsAffected() ; err != nil {
        logging.Errorf(err, "Failed deleting pipeline with id=%v, err=%v", pipeline_id, err)
        return false, err
    } else {
        return num_changed==1, nil
    }
}


func (db DB) AddStatusUpdateNeeded(pipeline_id int64, target_url, sha, name string) error {
    var existent_id int64
    if err := db.db.QueryRow("select pipeline_id from status_update_needed where pipeline_id=? and sha=? and name=?", pipeline_id, sha, name).Scan(&existent_id); err != nil {
        if err == sql.ErrNoRows {
            if _, err = db.db.Exec("insert into status_update_needed (pipeline_id, target_url, sha, name) VALUES (?,?,?,?)", pipeline_id, target_url, sha, name); err != nil {
                logging.Errorf(err, "Failed adding StatusUpdateNeeded: pipeline_id=%v target_url=%v, sha=%v, name=%v, err=%v ", pipeline_id, target_url, sha, name, err)
                return err
            }
        } else {
            logging.Errorf(err, "Failed adding StatusUpdateNeeded: pipeline_id=%v target_url=%v, sha=%v, name=%v, err=%v ", pipeline_id, target_url, sha, name, err)
            return err
        }
    }
    return nil
}

func (db DB) DelStatusUpdateNeeded(pipeline_id int64, target_url, sha, name string) error {
    if _, err := db.db.Exec("delete from status_update_needed where pipeline_id=? and sha=? and name=?", pipeline_id, sha, name); err != nil {
        logging.Errorf(err, "Failed deleting StatusUpdateNeeded pipeline_id=%v target_url=%v, sha=%v, name=%v, err=%v ", pipeline_id, target_url, sha, name, err)
        return err
    }
    return nil
}

func (db DB) GetAllStatusUpdateNeeded() ([]StatusUpdateNeeded, error) {
    if rows, err := db.db.Query("select pipeline_id, target_url, sha, name from status_update_needed"); err != nil {
        return nil, err
    } else {
        defer rows.Close()
        var ret []StatusUpdateNeeded
        for rows.Next() {
            var s StatusUpdateNeeded
            if err := rows.Scan(&s.PipelineID, &s.TargetURL, &s.SHA, &s.Name); err != nil {
                return nil, err
            }
            ret = append(ret, s)
        }
        return ret,nil
    }
}


func (db DB) AddCredential(gl_project_id int, job_id int, username string, password string, system string) error {
    if _, err := db.db.Exec("insert into access_tokens (gl_project_id, job_id, username, password, system) values (?,?,?,?,?) on duplicate key update username=?, password=?", gl_project_id, job_id, username, password, system, username, password); err != nil {
        logging.Errorf(err, "Failed adding credential for job_id=%v, username=%v, password=%v, system=%v, err=%v", job_id, username, password, system, err)
        return err
    }
    return nil
}

// returns (username, password, credtials_found, error)
func (db DB) GetCredential(job_id int, system string) (string, string, bool, error) {
    var username, password string
    if err := db.db.QueryRow("select username, password from access_tokens where job_id=? and system=?", job_id, system).Scan(&username, &password); err != nil {
        if err == sql.ErrNoRows {
            return "","", false, nil
        }
        return "","",false,err
    }
    return username,password,true,nil
}

// returns all credentials belonging to a `job_id`
func (db DB) GetCredentials(job_id int64) ([]Credential, error) {
    if rows, err := db.db.Query("select gl_project_id, job_id, username, password, system from access_tokens where job_id=?", job_id); err != nil {
        return nil, err
    } else {
        defer rows.Close()
        var ret []Credential
        for rows.Next() {
            var c Credential
            if err := rows.Scan(&c.GlProjectID, &c.JobID, &c.Username, &c.Password, &c.System); err != nil {
                return nil, err
            }
            ret = append(ret, c)
        }
        return ret,nil
    }
}

func (db DB) DeleteCredential(job_id int, system string) error {
    if _, err := db.db.Exec("delete from access_tokens where job_id=? and system=?", job_id, system); err != nil {
        return err
    }
    return nil
}

func (db DB) GetAllCredentials() ([]Credential, error) {
    if rows, err := db.db.Query("select gl_project_id, job_id, username, password, system from access_tokens"); err != nil {
        return nil, err
    } else {
        defer rows.Close()
        var ret []Credential
        for rows.Next() {
            var c Credential
            if err := rows.Scan(&c.GlProjectID, &c.JobID, &c.Username, &c.Password, &c.System); err != nil {
                return nil, err
            }
            ret = append(ret, c)
        }
        return ret,nil
    }
}

func (db DB) GetRegistryAllowPaths(repository_id int64) (map[string][]string, error) {
    rows, err := db.db.Query("select repo, include_pattern from allow_registry_access where repository_id=?", repository_id)
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    ret := make(map[string][]string)
    for rows.Next() {
        var repo, include_pattern string
        if err := rows.Scan(&repo, &include_pattern); err != nil {
            return nil, err
        }
        ret[repo] = append(ret[repo], include_pattern)
    }
    return ret,nil
}

func (db DB) AddRegistryAllowPaths(repository_id int64, repo, include_pattern string) error {
    if _, err := db.db.Exec("insert into allow_registry_access (repository_id, repo, include_pattern) values (?,?,?)", repository_id, repo, include_pattern); err != nil {
        logging.Errorf(err, "Failed adding row to allow_registry_access repository_id=%v repo=%v, include_pattern=%v, err=%v", repository_id, repo, include_pattern, err)
        return err
    }
    return nil
}

func (db DB) SaveJwtSessionId(db_id, sid string) {
    if _, err := db.db.Exec("update websessions set sid=? where id=?", sid, db_id); err != nil {
        logging.Error(err, "Failed saving SID")
    }
}

func (db DB) DeleteWebsession(sid string) {
    if _, err := db.db.Exec("delete from websessions where sid=?", sid); err != nil {
        logging.Errorf(err, "Failed deleting session by SID. given sid=%v", sid)
    }
}


func (db DB) GetRepositoryAccessForClaims(claims []string) []RepositoryAccess {
    var ret []RepositoryAccess
    if len(claims) < 1 {
        return nil
    }
    args := make([]interface{}, len(claims))
    for i, claim := range claims {
        args[i] = claim
    }

    stmt := fmt.Sprintf("select repository_id,MAX(access_level),claim from repository_access where claim in (?%v) group by repository_id", strings.Repeat(",?", len(args)-1)) //#nosec G201 -- just added ?,?,?
    // nosemgrep: gosec.G202-1
    rows, err := db.db.Query(stmt, args...) //#nosec G201 -- just added ?,?,?
    if err != nil {
        logging.Error(err, "Failed query for repository_access")
        return nil
    }
    defer rows.Close()

    for rows.Next() {
        var repo_id int64
        var access_level AccessLevelType
        var claim string
        if err := rows.Scan(&repo_id, &access_level, &claim); err != nil {
            logging.Error(err, "Failed rows.Scan() for repository_access")
            return nil
        }
        ret = append(ret, RepositoryAccess{RepoID: repo_id, AccessLevel: access_level, Claim: claim})
    }
    return ret
}

func (db DB) GetAccessLevelForRepoWithClaims(repo_id int64, claims []string) AccessLevelType {
    var access_level AccessLevelType
    if len(claims) < 1 {
        return NoAccess
    }

    args := make([]any, len(claims)+1)
    args[0] = repo_id
    for i, claim := range claims {
        args[i+1] = claim
    }

    stmt := fmt.Sprintf("select MAX(access_level) from repository_access where repository_id=? and claim in (?%v) group by repository_id", strings.Repeat(",?", len(claims)-1)) //#nosec G201 -- just added ?,?,?
    // nosemgrep: gosec.G202-1
    if err := db.db.QueryRow(stmt, args...).Scan(&access_level) /* #nosec G201 */; err != nil {
        if errors.Is(err, sql.ErrNoRows) == false {
            logging.Error(err, "Failed query for access_level")
        }
        return NoAccess
    } else {
        return access_level
    }
}

func (db DB) GetRepoClaimsForAccessLevel(repo_id int64, access_level AccessLevelType) []string {
    var ret []string
    rows, err := db.db.Query("select claim from repository_access where repository_id=? and access_level=?", repo_id, access_level)
    if err != nil {
        logging.Error(err, "Failed query for GetRepoClaimsForAccessLevel")
        return nil
    }
    defer rows.Close()

    for rows.Next() {
        var claim string
        if err := rows.Scan(&claim); err != nil {
            logging.Error(err, "Failed rows.Scan() for GetRepoClaimsForAccessLevel")
            return nil
        }
        ret = append(ret, claim)
    }
    return ret
}

func (db DB) GetRepoOwner(repo_id int64) string {
    owners := db.GetRepoClaimsForAccessLevel(repo_id, OwnerAccess)
    if len(owners) < 1 {
        logging.Warnf("Could not find an owner for repo_id=%v", repo_id)
        return ""
    }
    if len(owners) > 1 {
        logging.Warnf("Found more than one owner for repo_id=%v", repo_id)
    }
    return owners[0]
}
func (db DB) SetRepoOwner(repo_id int64, owner string) error {
    return db.replaceAccessLevelForClaims(repo_id, OwnerAccess, []string{owner})
}

func (db DB) GetRepoAdmins(repo_id int64) []string {
    return db.GetRepoClaimsForAccessLevel(repo_id, AdminAccess)
}

func (db DB) GetRepoManagers(repo_id int64) []string {
    return db.GetRepoClaimsForAccessLevel(repo_id, ManagerAccess)
}

func (db DB) ReplaceRepoAdmins(repo_id int64, new_admins []string) error {
    return db.replaceAccessLevelForClaims(repo_id, AdminAccess, new_admins)
}

func (db DB) ReplaceRepoManagers(repo_id int64, new_managers []string) error {
    return db.replaceAccessLevelForClaims(repo_id, ManagerAccess, new_managers)
}

func (db DB) replaceAccessLevelForClaims(repo_id int64, access_level AccessLevelType, new_claims []string) error {
    var err error
    if _, err = db.db.Exec("delete from repository_access where repository_id=? and access_level=?", repo_id, access_level); err != nil {
        logging.Errorf(err, "Failed deleting from repository_access with err=%v", err)
        return err
    }
    for _, claim := range new_claims {
        stmt := "insert into repository_access (repository_id, access_level, claim) values (?, ?, ?) on duplicate key update access_level=GREATEST(?, access_level)"
        if _, err = db.db.Exec(stmt, repo_id, access_level, claim, access_level) ; err != nil {
            logging.Errorf(err, "Failed adding to repository_access, err=%v", err)
            return err
        }
    }
    return nil
}


func (db DB) Close() error {
    return db.db.Close()
}

// helper functions to query single attributes for a given primary id
func (db DB) getRepositoryAttribute(repository_id int64, attribute string, ret any) {
    db.getAttribute("repository", repository_id, attribute, ret)
}
func (db DB) setRepositoryAttribute(repository_id int64, attribute string, new_value any) bool {
    return db.setAttribute("repository", repository_id, attribute, new_value)
}

func (db DB) getPipelineAttribute(pipeline_id int64, attribute string, ret any) {
    db.getAttribute("pipeline", pipeline_id, attribute, ret)
}
func (db DB) setPipelineAttribute(pipeline_id int64, attribute string, new_value any) bool {
    return db.setAttribute("pipeline", pipeline_id, attribute, new_value)
}

func (db DB) getAttribute(table string, primary_id int64, attribute string, ret any) {
    // internal function - we trust the input arguments
    // nosemgrep: gosec.G202-1
    row := db.db.QueryRow(fmt.Sprintf("select %v from %v where %v_id=?", attribute, table, table), primary_id)
    if err := row.Scan(ret); err != nil {
        logging.Warnf("ID=%v was not found in table=%v. Requested attribute=%v. Err=%v", primary_id, table, attribute, err)
    }
}
func (db DB) setAttribute(table string, primary_id int64, attribute string, new_value any) bool {
    // internal function - we trust the variables table and attribute
    // nosemgrep: gosec.G202-1
    if result, err := db.db.Exec(fmt.Sprintf("update %v set %v=? where %v_id=?", table, attribute, table), new_value, primary_id) ; err != nil {
        logging.Warnf("Could not update attribute=%v to new_value=%v in table=%v with primary_id=%v. err=%v", attribute, new_value, table, primary_id, err)
        return false
    } else {
        if num_changed, err := result.RowsAffected() ; err != nil {
            logging.Warnf("Error calling RowsAffected(). err=%v", err)
            return false
        } else {
            return num_changed == 1
        }
    }
}

func rows_to_slice[T any](rows *sql.Rows, err error) []T {
    if err != nil {
        logging.Error(err, "Failed query")
        return []T{}
    }
    defer rows.Close()
    var ret []T
    for rows.Next() {
        var field T
        if err := rows.Scan(&field); err != nil {
            logging.Error(err, "Failed scanning row")
            return []T{}
        }
        ret = append(ret, field)
    }
    return ret
}

func (db DB) getNextId(table string, startWithId int64) int64 {
    var id int64
    for {
        startWithId += 1
        // nosemgrep: gosec.G202-1
        row := db.db.QueryRow(fmt.Sprintf("select %v_id from %v where %v_id=?", table, table, table), startWithId)
        if err := row.Scan(&id); err != nil {
            if err == sql.ErrNoRows {
                return startWithId
            }
        }
    }
}

func (db DB) getRandomId(table string) int64 {
    var id int64
    for {
        // we do not need a cryptographically strong random id - it is not hidden for opensource projects anyway
        randId := rand.Int63n((1<<53))+1 // #nosec G404
        // nosemgrep: gosec.G202-1
        row := db.db.QueryRow(fmt.Sprintf("select %v_id from %v where %v_id=?", table, table, table), randId)
        if err := row.Scan(&id); err != nil && err == sql.ErrNoRows {
            return randId
        }
    }
}
