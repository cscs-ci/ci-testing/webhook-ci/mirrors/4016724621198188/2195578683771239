package util

import (
    "errors"
    "fmt"
    "os"
    "path/filepath"
    "time"

    "github.com/go-sql-driver/mysql"
    "gopkg.in/yaml.v3"

    "cscs.ch/cicd-ext-mw/logging"
)

type DBConfig struct {
    User string `yaml:"user"`
    Password string `yaml:"password"`
    Net string `yaml:"net"`
    Address string `yaml:"address"`
    DBName string `yaml:"dbname"`
}
type RedisConfig struct {
    Password string `yaml:"password"`
    Address string `yaml:"address"`
}
type ListenAddressConfig struct {
    Address string `yaml:"address"`
    Port int `yaml:"port"`
}
type GitlabConfig struct {
    Url string `yaml:"url"`
    MirrorsPath string `yaml:"mirrors_path"`
    ContainerBuilderServicePipelineId int64 `yaml:"container_builder_service_project"`
    UenvBuilderServicePipelineId int64 `yaml:"uenv_builder_service_project"`
    Token string `yaml:"token"`
}
type JFrogConfig struct {
    Token string `yaml:"token"`
    URL string `yaml:"url"`
    EMail string `yaml:"email"`
}
type SSHKeyPathConfig struct {
    Orig string `yaml:"orig"`
    Mirror string `yaml:"mirror"`
}
type TemplatesConfig struct {
    Dir string `yaml:"dir"`
    AlwaysInclude []string `yaml:"always_include"`
}
type SpackBuildcacheConfig struct {
    Cloud string `yaml:"cloud"`
    SignKeyPath string `yaml:"sign_key_path"`
}
type RunnerObserverConfig struct {
    Output string `yaml:"output"`
    Runners []int `yaml:"runners"`
}
type OpenIdConfig struct {
    AuthURL string `yaml:"auth_url"`
    TokenURL string `yaml:"token_url"`
    UserinfoURL string `yaml:"userinfo_url"`
    LogoutURL string `yaml:"logout_url"`
    JwksURL string `yaml:"jwks_url"`
    JwksURLApiGw string `yaml:"jwks_url_api_gw"`
    ClientID string `yaml:"client_id"`
    ClientSecret string `yaml:"client_secret"`
    OverrideCallback string `yaml:"override_callback"`
}
type CookieConfig struct {
    Secret string `yaml:"secret"`
    MaxAge int `yaml:"max_age"`
}
type ReportingConfig struct {
    URL string `yaml:"url"`
    Datastream Datastream `yaml:"datastream"`
    Enabled bool `yaml:"enabled"`
}
type Datastream struct {
    Type string `yaml:"type"`
    Dataset string `yaml:"dataset"`
    Namespace string `yaml:"namespace"`
}
type HeartbeatConfig struct {
    URL string `yaml:"url"`
    PeriodSec int `yaml:"period_sec"`
}
type Config struct {
    DB DBConfig `yaml:"db"`
    Redis RedisConfig `yaml:"redis"`
    Listen ListenAddressConfig `yaml:"listen"`
    MaxBodySize int64 `yaml:"max_body_size"`
    BaseURL string `yaml:"baseurl"`
    URLPrefix string `yaml:"urlprefix"`
    RequestsLogDir string `yaml:"requests_log_dir"`
    ErrorsDir string `yaml:"errors_dir"`
    Templates TemplatesConfig `yaml:"templates"`
    Gitlab GitlabConfig `yaml:"gitlab"`
    RegistryPath string `yaml:"registry_path"`
    EnableCron bool `yaml:"enable_cron"`
    ProjectRegistration []string `yaml:"project_registration"`
    NotifyURL string `yaml:"notify_url"`
    JFrog JFrogConfig `yaml:"jfrog"`
    SSHKeyPath SSHKeyPathConfig `yaml:"ssh_key_path"`
    SpackBuildcache SpackBuildcacheConfig `yaml:"spack_buildcache"`
    RunnerObserver RunnerObserverConfig `yaml:"runner_observer"`
    OpenIdConfig `yaml:"openid"`
    CookieConfig `yaml:"cookie"`
    ReportingConfig `yaml:"reporting"`
    HeartbeatConfig `yaml:"heartbeat"`
}


func ReadConfig(path string) Config {
    logger := logging.Get()
    data, err := os.ReadFile(filepath.Clean(path))
    if err != nil {
        logger.Error().Err(err).Msgf("Error opening config file `%v`", path)
        panic(err)
    }
    var config Config
    if err := yaml.Unmarshal(data, &config); err != nil {
        logger.Error().Err(err).Msg("Error unmarshalling config from yaml file")
        panic(err)
    }
    logger.Debug().Msgf("Read config: %+v", config)

    // overwrite secrets from environment variables, if they are set (also overwrite even if the value is an empty string - if the env-variable exists, it will overwrite)
    if envvar, ok := os.LookupEnv("CIEXT_OPENID_CLIENT_SECRET"); ok {
        config.OpenIdConfig.ClientSecret = envvar
    }
    if envvar, ok := os.LookupEnv("CIEXT_JFROG_TOKEN"); ok {
        config.JFrog.Token = envvar
    }
    if envvar, ok := os.LookupEnv("CIEXT_GITLAB_TOKEN"); ok {
        config.Gitlab.Token = envvar
    }
    if envvar, ok := os.LookupEnv("CIEXT_DATABASE_PASSWORD"); ok {
        config.DB.Password = envvar
    }
    if envvar, ok := os.LookupEnv("CIEXT_COOKIE_SECRET"); ok {
        config.CookieConfig.Secret = envvar
    }
    if envvar, ok := os.LookupEnv("REDIS_PASSWORD"); ok {
        config.Redis.Password = envvar
    }

    // sanity checks that config file has everything we want
    if config.MaxBodySize <= 0 {
        logger.Error().Msgf("Read MaxBodySize=%v from config file, which is not a valid value. Please specify a value > 0.", config.MaxBodySize)
        panic("Incorrect MaxBodySize config")
    }

    if config.BaseURL == "" {
        logger.Error().Msgf("Read BaseURL=%q from config file, which is not a valid value. Please specify a non-empty value.", config.BaseURL)
        panic("Incorrect BaseURL")
    }
    if config.URLPrefix == "" {
        logger.Error().Msgf("Read URLPrefix=%q from config file, which is not a valid value. Please specify a non-empty value.", config.URLPrefix)
        panic("Incorrect URLPrefix config")
    }

    if stat, err := os.Stat(config.Templates.Dir); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msgf("Read Templates.Dir=%q from config file, which does not point to an existing directory. Please specify a valid directory", config.Templates.Dir)
        panic("Incorrect Templates.Dir")
    }
    // Templates.AlwaysInclude should not be empty, and every file must exist
    if len(config.Templates.AlwaysInclude) == 0 {
        logger.Error().Msgf("Empty list Templates.AlwaysInclude. This is probably wrong")
        panic("Incorrect Templates.AlwaysInclude")
    }
    for _, template_path := range config.Templates.AlwaysInclude {
        if stat, err := os.Stat(fmt.Sprintf("%v/%v", config.Templates.Dir, template_path)); errors.Is(err, os.ErrNotExist) || stat.Mode().IsRegular() == false {
            logger.Error().Msgf("Could not find template %v", template_path)
            panic("Template not found")
        }
    }

    if config.RegistryPath == "" {
        logger.Error().Msgf("Read RegistryPath=%q from config file which is not a valid value. Please specify a non-empty value.", config.RegistryPath)
        panic("Incorrect RegistryPath config")
    }

    if config.JFrog.Token == "" || config.JFrog.URL == "" || config.JFrog.EMail == "" {
        logger.Error().Msg("The config values for JFrog are invalid. Mandatory fields are Token, URL, EMail, and they are not allowed to be empty.")
        panic("Incorrect JFrog config")
    }

    if config.Gitlab.Url == ""  || config.Gitlab.MirrorsPath == "" || config.Gitlab.Token == "" || config.Gitlab.ContainerBuilderServicePipelineId ==  0  || config.Gitlab.UenvBuilderServicePipelineId == 0 {
        logger.Error().Msg("Invalid Gitlab config. Mandatory fields are Url, MirrorsPath, Token, ContainerBuilderServicePipelineId, UenvBuilderServicePipelineId")
        panic("Incorrect Gitlab config ")
    }

    if config.SpackBuildcache.Cloud == "" || config.SpackBuildcache.SignKeyPath == "" {
        logger.Error().Msg("Invalid SpackBuildcache config. Mandatory fields are Cloud and SignKeyPath")
        panic("Incorrect SpackBuildcache config")
    }
    if stat, err := os.Stat(config.SSHKeyPath.Orig); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("SSHKeyPath.Orig does not point to a directory")
        panic("SSHKeyPath.Orig does not point to a directory")
    }
    if stat, err := os.Stat(config.SSHKeyPath.Mirror); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("SSHKeyPath.Mirror does not point to a directory")
        panic("SSHKeyPath.Mirror does not point to a directory")
    }

    if config.ErrorsDir == "" {
        logger.Error().Msg("Invalid ErrorsDir config. It is not allowed to be unset")
        panic("IncorrectErrorsDir config")
    }
    if stat, err := os.Stat(config.ErrorsDir); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("ErrorsDir does not point to an existing directory")
        panic("ErrorsDir does not point to an existing director")
    }

    if config.RunnerObserver.Output == "" || len(config.RunnerObserver.Runners) == 0 {
        logger.Error().Msg("Invalid RunnerObserver config. Mandatory fields are Output and Runners")
        panic("Incorrect RunnerObserver config")
    }

    if config.RequestsLogDir == "" {
        logger.Error().Msg("Invalid RequestsLogDir. It cannot be an empty string")
        panic("Incorrect RequestsLogDir")
    }
    if stat, err := os.Stat(config.RequestsLogDir); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("RequestsLogDir does not point to an existing directory")
        panic("RequestsLogDir ddoes not point to a directory")
    }

    if config.OpenIdConfig.AuthURL == "" || config.OpenIdConfig.TokenURL == "" || config.OpenIdConfig.UserinfoURL == "" || config.OpenIdConfig.LogoutURL == "" || config.OpenIdConfig.JwksURL == "" || config.OpenIdConfig.JwksURLApiGw == "" || config.OpenIdConfig.ClientID == "" || config.OpenIdConfig.ClientSecret == "" {
        logger.Error().Msg("Invalid OpenIdConfig. AuthURL, TokenURL, UserinfoURL, LogoutURL, JwksURL, JwksURLApiGw, ClientID, ClientSecret are mandatory fields")
        panic("Incorrect OpenIdConfig")
    }
    if config.CookieConfig.MaxAge == 0 || config.CookieConfig.Secret == "" {
        logger.Error().Msg("Invalid CookieConfig. Secret and MaxAge are mandatory fields")
        panic("Incorrect CookieConfig")
    }

    if config.ReportingConfig.Enabled && (config.ReportingConfig.URL == "" || config.ReportingConfig.Datastream.Type == "" || config.ReportingConfig.Datastream.Dataset == "" || config.ReportingConfig.Datastream.Namespace == "") {
        logger.Error().Msg("Invalid ReportingConfig. URL and Datastream must not have any empty strings")
        panic("Incorrect ReportingConfig")
    }

    if config.Redis.Address == "" {
        logger.Error().Msg("Invalid RedisConfig. Address is missing")
        panic("Incorrect RedisConfig")
    }

    return config
}

func (cfg *Config) GetRegistryPathForRepo(repository_id int64) string {
    return fmt.Sprintf("%v/%v", cfg.RegistryPath, repository_id)
}

func (cfg *Config) GetSSHKeyPath(repo_or_pipeline_id int64, mirror bool, public bool) string {
    var path string
    if mirror {
        path = fmt.Sprintf("%v/key_%v", cfg.SSHKeyPath.Mirror, repo_or_pipeline_id)
    } else {
        path = fmt.Sprintf("%v/key_%v", cfg.SSHKeyPath.Orig, repo_or_pipeline_id)
    }
    if public {
        path += ".pub"
    }
    return filepath.Clean(path)
}

func (cfg *Config) GetSshKey(repo_or_pipeline_id int64, mirror bool, pubkey bool) ([]byte, error) {
    // nosemgrep: gosec.G304-1
    return os.ReadFile(cfg.GetSSHKeyPath(repo_or_pipeline_id, mirror, pubkey))
}

func (cfg *Config) GetSpackSignKey() ([]byte, error) {
    return os.ReadFile(filepath.Clean(cfg.SpackBuildcache.SignKeyPath))
}

func (cfg *Config) GetDBPath() string {
    mysqlConfig := mysql.NewConfig()
    mysqlConfig.User = cfg.DB.User
    mysqlConfig.Passwd = cfg.DB.Password
    mysqlConfig.Addr = cfg.DB.Address
    mysqlConfig.DBName = cfg.DB.DBName
    mysqlConfig.Net = cfg.DB.Net
    mysqlConfig.ParseTime = true
    mysqlConfig.Loc = time.Local
    return mysqlConfig.FormatDSN()
}

func (cfg *Config) GetRequestLogPath(reqId string) string {
    return filepath.Clean(fmt.Sprintf("%v/req-%v.log", cfg.RequestsLogDir, reqId))
}
