package util;

import (
    "crypto/sha256"
    "encoding/hex"
    "errors"
    "fmt"
    "io"
    "io/ioutil"
    "net/url"
    "os"
    "strings"
    "sync"
    "time"

    "github.com/go-co-op/gocron"
    "github.com/go-playground/retry"
    "github.com/xanzy/go-gitlab"
    "github.com/rs/zerolog"
    "github.com/go-git/go-git/v5"
    gitconfig "github.com/go-git/go-git/v5/config"
    "github.com/go-git/go-git/v5/plumbing"
    "github.com/go-git/go-git/v5/plumbing/transport"
    gitssh "github.com/go-git/go-git/v5/plumbing/transport/ssh"

    "cscs.ch/cicd-ext-mw/logging"
)

var locked_repo_dirs sync.Map

type CronScheduler struct {
    glserver *gitlab.Client
    db DB
    config Config
}

func NewCronScheduler(glserver *gitlab.Client, db DB, config Config) CronScheduler {
    return CronScheduler{glserver, db, config}
}

func (c CronScheduler) ScheduleJobs() {
    scheduler := gocron.NewScheduler(time.Local)
    scheduler.StartAsync()
    jobsBySID := make(map[int64]schedulerData)
    // poll for cron schedules - This is not optimal, and we should only reread the config when it was changed, but we would need an IPC mechanism first (e.g. Redis)
    for {
        schedules_db := c.db.GetAllPipelineSchedules()
        schedules_db_id_map := map[int64]bool{}
        for _, sdb := range schedules_db {
            // sdb is the schedule as stored in database - i.e. the truth
            if jobDataRunning, exists := jobsBySID[sdb.ScheduleID]; exists {
                // this cron schedule is already running, we need to check if the schedule has changed and reschedule appropriately
                if jobDataRunning.dbData.CronSchedule != sdb.CronSchedule {
                    // the schedule has changed - cancel old job - schedule new job
                    logging.Debugf("Cron scheduling times for schedule_id=%v has changed. Old schedule=%v, new schedule=%v - rescheduling", sdb.ScheduleID, jobDataRunning.dbData.CronSchedule, sdb.CronSchedule)
                    scheduler.RemoveByReference(jobDataRunning.job)
                    c.schedule_job(sdb, scheduler, jobsBySID)
                } else if jobDataRunning.dbData.Ref != sdb.Ref {
                    // Reference has changed, keep track of it
                    logging.Debugf("Cron trigger ref has changed for schedule_id=%v. Old ref=%v, new ref=%v", sdb.ScheduleID, jobDataRunning.dbData.Ref, sdb.Ref)
                    entry := jobsBySID[sdb.ScheduleID]
                    entry.dbData = sdb
                }
            } else {
                // this cron schedule has not yet been seen/started
                c.schedule_job(sdb, scheduler, jobsBySID)
            }

            // keep track of ids still existing in the DB
            schedules_db_id_map[sdb.ScheduleID] = true
        }

        // stop all cron jobs that have been deleted, but are scheduled
        for scheduler_id, job := range jobsBySID {
            if _, exists := schedules_db_id_map[scheduler_id]; exists == false {
                // deleted from DB, i.e. deleted in the CI setup page --> stop job
                scheduler.RemoveByReference(job.job)
            }
        }

        // reread schedules from DB every 10 minutes
        time.Sleep(10*time.Minute)
    }
}

func (c *CronScheduler) schedule_job(schedule_db ScheduleDataNoVars, scheduler *gocron.Scheduler, jobsBySID map[int64]schedulerData) {
    job, err := scheduler.Cron(schedule_db.CronSchedule).Do(func(){
        c.trigger_pipeline(schedule_db.ScheduleID)
    })
    if err != nil {
        logging.Error(err, "Failed scheduling job")
    } else {
        jobsBySID[schedule_db.ScheduleID] = schedulerData{schedule_db, job}
    }
}

func (c *CronScheduler) trigger_pipeline(schedule_id int64) {
    logger := logging.Get()
    schedule_data, pipeline_id := c.db.GetPipelineSchedule(schedule_id)

    // clone or open repository - we assume here that no other task is currently working on the repository
    // this assumption is valid as long as this is cloning to a different directory as the webhook_ci handler
    repo_id := c.db.GetRepositoryId(pipeline_id)
    target_path := fmt.Sprintf("/home/tmp/scheduled/%v", repo_id)
    LockRepoDir(target_path, logger)
    defer UnlockRepoDir(target_path, logger)
    repo, err := GitCloneOrOpen(&c.db, &c.config, target_path, repo_id, true, logger, nil)
    if err != nil {
        logger.Error().Err(err).Msgf("Failed cloning repository error=%v", err)
        return
    }
    if err := GitFetch(&c.db, &c.config, repo, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/*:refs/heads/*")}, repo_id, logger, nil); err != nil {
        logger.Error().Err(err).Msgf("Failed fetching updates from remote. Error=%v", err)
        return
    }
    hash, err := repo.ResolveRevision(plumbing.Revision(schedule_data.Ref))
    if err != nil {
        logger.Error().Err(err).Msgf("Failed getting SHA for reference %v", schedule_data.Ref)
        return
    }
    notification_context := schedule_data.Variables["CSCS_NOTIFICATION_CONTEXT"]
    if notification_context == "" {
        notification_context = db.GetPipelineName(pipeline_id)
    }
    if err := EnsureMirrorRepo(&c.db, &c.config, c.glserver, logger, repo_id, pipeline_id); err != nil {
        NotifyError(fmt.Append(nil, "error: ", err, ".<br>"), hash.String(), notification_context, "Error creating mirror repository", schedule_data.Ref, true, repo_id, &c.config, &c.db, logger)
        logger.Error().Err(err).Msgf("Failed creating mirror repository error=%v", err)
        return
    }
    refspecs := []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+refs/heads/%v:refs/heads/%v", schedule_data.Ref, schedule_data.Ref))}
    if err := GitPush(&c.db, &c.config, repo, refspecs, repo_id, pipeline_id, logger, nil); err != nil {
        NotifyError(fmt.Append(nil, "error: ", err, ".<br>"), hash.String(), notification_context, "Error pushing to mirror repository", schedule_data.Ref, true, repo_id, &c.config, &c.db, logger)
        logger.Error().Err(err).Msgf("Failed pushing branches to mirror repository. error=%v", err)
        return
    }
    if _, err := TriggerPipelineWithRetry(c.glserver, &c.config, &c.db, pipeline_id, schedule_data.Ref, hash.String(), schedule_data.Variables, logger); err != nil {
        logger.Warn().Err(err).Msgf("Failed triggering pipeline for schedule_id=%v", schedule_id)
    }
}


func GitCloneOrOpen(db *DB, config *Config, target_path string, repo_id int64, bare_clone bool, logger *zerolog.Logger, progress *os.File) (*git.Repository, error) {
    if progress == nil {
        progress = os.Stdout
    }
    if repo, err := git.PlainOpen(target_path); err != nil {
        // could not open repo, we need to clone it
        clone_options := git.CloneOptions{
            URL: GitUrlOrig(db, logger, repo_id),
            Auth: GitAuthOrig(config, logger, repo_id),
            Progress: progress,
        }
        logger.Info().Msgf("Cloning to %v with options=%+v", target_path, clone_options)
        return git.PlainClone(target_path, bare_clone, &clone_options)
    } else {
        logger.Info().Msgf("path=%v exists already, using existing git repository", target_path)
        return repo, nil
    }
}

func GitFetch(db *DB, config *Config, repo *git.Repository, fromRepo string, refspecs []gitconfig.RefSpec, repo_id int64, logger *zerolog.Logger, progress *os.File) error {
    if progress == nil {
        progress = os.Stdout
    }
    fetchOptions := git.FetchOptions{
        Force: true,
        RemoteURL: GitUrlOrig(db, logger, repo_id),
        Auth: GitAuthOrig(config, logger, repo_id),
        RefSpecs: refspecs,
        Progress: progress,
    }
    if fromRepo != "" {
        fetchOptions.RemoteURL = fromRepo
    }
    logger.Debug().Msgf("Fetching fetchOptions=%#v", fetchOptions)
    err := repo.Fetch(&fetchOptions)
    if err != nil && err != git.NoErrAlreadyUpToDate {
        return err
    }
    return nil
}

func GitPush(db *DB, config *Config, repo *git.Repository, refspecs []gitconfig.RefSpec, repo_id, pipeline_id int64, logger *zerolog.Logger, progress *os.File) error {
    if progress == nil {
        progress = os.Stdout
    }
    o := &git.PushOptions{
        Force: true,
//            Prune: true, // do not enable Prune, seems to fail pre-receive hook, without further information
        Progress: progress,
        RefSpecs: refspecs,
        Options: map[string]string{"ci.skip":"1"},
        RemoteURL: GitUrlMirror(config, repo_id, pipeline_id),
        Auth: GitAuthMirror(config, pipeline_id, logger),
    }
    logger.Debug().Msgf("Using push options %#v", o)
    if err := repo.Push(o); err != nil {
        if err == git.NoErrAlreadyUpToDate {
            logger.Info().Msgf("Mirror repository is already up to date. Nothing to push")
        } else if errors.Is(err, io.EOF) {
            logger.Error().Err(err).Msg("Failed pushing to mirror repository with EOF error. Retrying...")
            time.Sleep(60*time.Second)
            return GitPush(db, config, repo, refspecs, repo_id, pipeline_id, logger, progress)
        } else {
            return err
        }
    }
    return nil
}

func GitUrlOrig(db *DB, logger *zerolog.Logger, repo_id int64) string {
    if db.IsPrivateRepo(repo_id) {
        // transform https://example.com/namespace/project --> example.com:namespace/project
        if u, err := url.Parse(db.GetRepoUrl(repo_id)); err != nil {
            logger.Error().Err(err).Msgf("Failed parsing repository url")
            return ""
        } else {
            return fmt.Sprintf("%v:%v", u.Hostname(), u.Path[1:])
        }
    } else {
        return db.GetRepoUrl(repo_id)
    }
}

func GitAuthOrig(config *Config, logger *zerolog.Logger, repo_id int64) transport.AuthMethod {
    if db.IsPrivateRepo(repo_id) {
        privkey_path := config.GetSSHKeyPath(repo_id, false, false)
        if publicKeys, err := gitssh.NewPublicKeysFromFile("git", privkey_path, "") ; err != nil {
            logger.Error().Err(err).Msgf("Error reading ssh keyfile")
            return nil
        } else {
            return publicKeys
        }
    }
    return nil
}

func GitUrlMirror(config *Config, repo_id, pipeline_id int64) string {
    return fmt.Sprintf("%v:%v/%v/%v", strings.Replace(config.Gitlab.Url, "https://", "", 1), config.Gitlab.MirrorsPath, repo_id, pipeline_id)
}

func GitAuthMirror(config *Config, pipeline_id int64, logger *zerolog.Logger) transport.AuthMethod {
    privkey_path := config.GetSSHKeyPath(pipeline_id, true, false)
    if publicKeys, err := gitssh.NewPublicKeysFromFile("git", privkey_path, "") ; err != nil {
        logger.Error().Err(err).Msgf("Error reading ssh keyfile")
        return nil
    } else {
        return publicKeys
    }
}

func EnsureMirrorRepo(db *DB, config *Config, glserver *gitlab.Client, logger *zerolog.Logger, repo_id, pipeline_id int64) error {
    mirror_repo_name := fmt.Sprintf("%v-%v", db.GetRepositoryName(repo_id), db.GetPipelineName(pipeline_id))
    if mirror_project, _, err := glserver.Projects.GetProject(fmt.Sprintf("%v/%v/%v", config.Gitlab.MirrorsPath, repo_id, pipeline_id), nil) ; err != nil {
        logger.Info().Err(err).Msgf("No mirror project found for %v. Creating a new mirror project", mirror_repo_name)
        var visibility = gitlab.PrivateVisibility
        if !db.IsPrivateRepo(repo_id) {
            visibility = gitlab.PublicVisibility
        }
        var group *gitlab.Group
        var err error
        if group, _, err = glserver.Groups.GetGroup(fmt.Sprintf("%v/%v", config.Gitlab.MirrorsPath, repo_id), &gitlab.GetGroupOptions{}); err != nil {
            logger.Info().Err(err).Msgf("Could not find group for mirror repositories. Creating a new one")
            if mirrorGroup, _, err := glserver.Groups.GetGroup(fmt.Sprintf("%v", config.Gitlab.MirrorsPath), &gitlab.GetGroupOptions{}); err != nil {
                logger.Error().Err(err).Msg("Could not get top-level mirror groupd from gitlab. Cannot create  mirror repository")
                return err
            } else {
                group, _, err = glserver.Groups.CreateGroup(&gitlab.CreateGroupOptions{
                    Name: gitlab.String(fmt.Sprintf("%v-%v", db.GetRepositoryName(repo_id), repo_id)),
                    Path: gitlab.String(fmt.Sprintf("%v", repo_id)),
                    ParentID: &mirrorGroup.ID,
                    // deprecated EmailsDisabled and gitlab fails with a 500-internal server errror when set
                    // the new field is called EmailsEnabled, but it is not added to go-gitlab yet
                    //EmailsDisabled: gitlab.Bool(true),
                    Visibility: &visibility,
                })
                if err != nil {
                    return err
                }
            }
        }
        new_project, _, err := glserver.Projects.CreateProject(&gitlab.CreateProjectOptions{
            Name: &mirror_repo_name,
            Path: gitlab.String(fmt.Sprintf("%v", pipeline_id)),
            Description: gitlab.String(fmt.Sprintf("Mirror of %v", db.GetRepoUrl(repo_id))),
            NamespaceID: &group.ID,
            PublicBuilds: gitlab.Bool(true), // this does not allow public access to the build logs for private projects though (True by default anyway, just here for reference)
            InitializeWithReadme: gitlab.Bool(true), // initialize with a readme, which creates a default branch (and a branch protection) such that we can delete the branch protection directly
            Visibility: &visibility,
            EmailsDisabled: gitlab.Bool(true),
            CIConfigPath: gitlab.String(db.GetCIEntrypoint(pipeline_id)),
        })
        if err != nil {
            return err
        }
        db.UpdateMirrorUrl(pipeline_id, new_project.WebURL)
        _, _, err = glserver.Projects.AddProjectHook(new_project.ID, &gitlab.AddProjectHookOptions{
            URL: gitlab.String(BuildCIURL("/webhook_gitlab_pipeline?repository_id=%v&pipeline_id=%v", config, repo_id, pipeline_id)),
            PipelineEvents: gitlab.Bool(true),
            JobEvents: gitlab.Bool(true),
            PushEvents: gitlab.Bool(false),
            Token: gitlab.String(db.GetMirrorWebhookSecret(pipeline_id)),
        })
        if err != nil {
            logger.Error().Err(err).Msg("Error setting up webhooks in new project mirror")
        }
        _, _, err = glserver.ProjectVariables.CreateVariable(new_project.ID, &gitlab.CreateProjectVariableOptions{
            Key: gitlab.String("CSCS_REGISTRY_PATH"),
            Value: gitlab.String(fmt.Sprintf("%v/%v", config.RegistryPath, repo_id)),
        })
        if err != nil {
            logger.Error().Err(err).Msg("Error creating variable in new project mirror")
        }

        // create SSH keyfiles
        if ssh_pubkey, err := CreateSshKeys(config.GetSSHKeyPath(pipeline_id, true, false)) ; err != nil {
            logger.Error().Err(err).Msg("Failed creating SSH keys")
        } else {
            _,_,err := glserver.DeployKeys.AddDeployKey(new_project.ID, &gitlab.AddDeployKeyOptions{
                Key: gitlab.String(string(ssh_pubkey)),
                Title: gitlab.String("CSCS-CI"),
                CanPush: gitlab.Bool(true),
            })
            if err != nil {
                logger.Error().Err(err).Msg("Failed adding deploy key to mirror project")
            }
        }

        // delete branch protection - retry until at least one protected branch is found
        for retry := 0; retry<10; retry++ {
            if protected_branches, _, err := glserver.ProtectedBranches.ListProtectedBranches(new_project.ID, &gitlab.ListProtectedBranchesOptions{}); err != nil {
                logger.Error().Err(err).Msg("Could not get protected branches from new project mirror")
            } else {
                logger.Debug().Msgf("protected_branches=%#v", protected_branches)
                if len(protected_branches) == 0 {
                    // no protected branches found - this is unusual, we will retry
                    time.Sleep(3*time.Second)
                    continue
                }
                for _, branch := range protected_branches {
                    logger.Debug().Msgf("Unprotecting branch %v in mirror repository", branch.Name)
                    if _, err := glserver.ProtectedBranches.UnprotectRepositoryBranches(new_project.ID, branch.Name); err != nil {
                        logger.Error().Err(err).Msgf("Failure unprotecting branch=%v", branch.Name)
                    }
                }
                // gitlab needs some time to unprotect the branches - sleep a bit
                time.Sleep(3*time.Second)
                break
            }
        }
        logger.Debug().Msgf("successfully created mirror repository %v", new_project.Name)
    } else {
        logger.Info().Msgf("Mirror repo %v exists already.", mirror_project.Name)
    }
    return nil
}


func TriggerPipelineWithRetry(glserver *gitlab.Client, config *Config, db *DB, pipeline_id int64, ref, sha string, variables map[string]string, logger *zerolog.Logger) (int, error) {
    max_attempts := uint16(1000) // really high number, because some errors have to be retried very often
    // only retry once for unknown errors - it is unlikely that they are fixed by retrying often enough (they are not really unknown, but rather in the category like "Missing CI file", "unknown key in extendes", "no jobs in yml"
    max_attempts_unknown_error := uint16(1)
    max_attempts_missing_ref := uint16(10) // retry more often if gitlab claims that the ref is missing (gitlab likes to claim this, when a new branch has just been pushed)
    bail_out := false
    const max_wait_time_seconds = 300

    const error_ref_not_found = "Reference not found"
    const error_too_many_jobs = "Project exceeded the allowed number of jobs in active pipelines"
    const error_timeout = "could not be fetched because of a timeout error"

    var triggered_pipeline_id int
    return triggered_pipeline_id, retry.Run(max_attempts, func() (bool, error) {
        var err error
        triggered_pipeline_id, err = TriggerPipeline(glserver, config, db, pipeline_id, ref, variables)
        return bail_out, err
    }, func(attempt uint16, err error) {
        bail_out = attempt >= max_attempts_unknown_error
        if strings.Index(err.Error(), error_ref_not_found) != -1 || strings.Index(err.Error(), error_timeout) != -1 {
            bail_out = attempt >= max_attempts_missing_ref
        } else if strings.Index(err.Error(), error_too_many_jobs) != -1 {
            bail_out = attempt == max_attempts
        }

        seconds_to_retry := min(20*attempt, max_wait_time_seconds)
        additional_info := fmt.Sprintf("Retrying in %v seconds", seconds_to_retry)
        if bail_out {
            additional_info = "This error is final - No more retries will be attempted."
        }
        logger.Warn().Err(err).Msgf("Failed triggering pipeline %v. Error=%v, attempt=%v bail_out=%v", db.GetPipelineName(pipeline_id), err, attempt, bail_out)

        if config.Gitlab.ContainerBuilderServicePipelineId == pipeline_id || config.Gitlab.UenvBuilderServicePipelineId == pipeline_id {
            // special case where this is the container/uenv builder service - we do not send a notification in this case to the original repository
            // we do not send a notification, because the commit does not exist in the original repository
            logger.Debug().Msg("Not sending a notification since this is the container/uenv-builder-service")
        } else if config.Gitlab.UenvBuilderServicePipelineId == pipeline_id {
            logger.Debug().Msg("Not sending a notification since this is the uenv-builder-service")
        } else {
            notification_context := variables["CSCS_NOTIFICATION_CONTEXT"]
            if notification_context == "" {
                notification_context = db.GetPipelineName(pipeline_id)
            }
            NotifyError(fmt.Append(nil, "error: ", err, ".<br>", additional_info), sha, notification_context, "Error triggering pipeline", ref, bail_out, db.GetRepositoryId(pipeline_id), config, db, logger)
        }

        if !bail_out {
            time.Sleep(time.Duration(seconds_to_retry)*time.Second)
        }
    })
}

func TriggerPipeline(glserver *gitlab.Client, config *Config, db *DB, pipeline_id int64, ref string, variables map[string]string) (int, error) {
    repo_id := db.GetRepositoryId(pipeline_id)
    all_vars := make(map[string]string)
    all_vars = merge_maps(all_vars, repo_vars(repo_id, db))
    all_vars = merge_maps(all_vars, pipeline_vars(pipeline_id, db))
    all_vars = merge_maps(all_vars, variables)
    all_vars = merge_maps(all_vars, ci_impl_vars(repo_id, config, db)) // implementation details will always overwrite, because the user should not use CSCS_CI_* as key
    pipeline, _, err := glserver.Pipelines.CreatePipeline(fmt.Sprintf("%v/%v/%v", config.Gitlab.MirrorsPath, repo_id, pipeline_id), &gitlab.CreatePipelineOptions{
        Ref: gitlab.String(ref),
        Variables: convert_as_gitlab_vars(all_vars),
    })
    if err != nil {
        return 0, err
    }
    return pipeline.ID, err
}


// error_final marks if this is a transient error (i.e. it will be retried automatically), or a final error (i.e. middleware will not attempt to retry anymore)
// if error_final == false, then we do not notify with an error, but rather with a pending message, to indicate that the pipeline is still running (in the sense that it will be restarted sooner or later)
// Setting a pipeline to failed state breaks auto-merge features of git providers, because they only evaluate at the very first success/failure message and do not consider any further status changes
// i.e. a pipeline that goes through "running --> failed --> running --> success" would not be merged automatically, because the merge-queue / auto-merge is disabled at the first failed message
func NotifyError(err_msg []byte, sha string, notification_name, desc, ref string, error_final bool, repo_id int64, config *Config, db *DB, logger *zerolog.Logger) {
    glstatus := GlStatusFailed
    if error_final == false {
        glstatus = GlStatusRunning
    }
    s256 := sha256.Sum256(err_msg)
    html_file := fmt.Sprintf("%v.html", hex.EncodeToString(s256[:]))
    // nosemgrep: gosec.G306-1
    if write_err := ioutil.WriteFile(fmt.Sprintf("%v/%v", config.ErrorsDir, html_file), err_msg, 0644) /* #nosec G306 -- file permissions are fine */; write_err != nil {
        logger.Error().Err(write_err).Msgf("Failed writing error html-file=%v", html_file)
    }
    target_url := BuildCIURL("/pipeline/results/error/%v", config, html_file)
    if resp, err := NotifyRepo(GetNotificationUrl(db.GetRepoUrl(repo_id), sha), glstatus, target_url, notification_name, db.GetNotificationToken(repo_id), desc, ref); err != nil {
        logger.Error().Err(err).Msgf("Failed notifying repository. err=%v", err)
    } else if resp == nil {
        // this is ok, notification context is set to discard notification
    } else if err := CheckResponse(resp); err != nil {
        // most probably an incorrect notification token
        logger.Warn().Err(err).Msgf("Failed notifiying original repository %v", db.GetRepositoryName(repo_id))
    }
}

func LockRepoDir(repo_path string, logger *zerolog.Logger) {
    for {
        if _, loaded := locked_repo_dirs.LoadOrStore(repo_path, true); loaded {
            logger.Info().Msgf("Path=%v is currently locked. Waiting 1s", repo_path)
            time.Sleep(1*time.Second)
        } else {
            logger.Info().Msgf("Path=%v has been successfully locked", repo_path)
            return
        }
    }
}
func UnlockRepoDir(repo_path string, logger *zerolog.Logger) {
    locked_repo_dirs.Delete(repo_path)
    logger.Info().Msgf("Path=%v has been successfully unlocked", repo_path)
}

// merges all keys from `b` to `a`.
// Duplicate keys overwrite values existing values in `a`, i.e. b[k] == a[k] for all k in keys(b) after merge
func merge_maps(a, b map[string]string) map[string]string {
    for k,v := range b {
        a[k] = v
    }
    return a
}


func repo_vars(repo_id int64, db *DB) map[string]string {
    ret := make(map[string]string)
    for _, v := range db.GetRepositoryVariables(repo_id) {
        ret[v.Key] = v.Value
    }
    return ret
}
func pipeline_vars(pipeline_id int64, db *DB) map[string]string {
    ret := make(map[string]string)
    for _, v := range db.GetPipelineVariables(pipeline_id) {
        ret[v.Key] = v.Value
    }
    return ret
}
func ci_impl_vars(repo_id int64, config *Config, db *DB) map[string]string {
    orig_clone_url := GitUrlOrig(db, logging.Get(), repo_id)
    if strings.HasPrefix(orig_clone_url, "http") == false {
        orig_clone_url = "git@" + orig_clone_url
    }
    return map[string]string {
        "CSCS_CI_MW_URL": BuildCIURL("", config),
        "CSCS_CI_DEFAULT_SLURM_ACCOUNT": db.GetF7tSlurmAccount(repo_id),
        "CSCS_CI_ORIG_CLONE_URL": orig_clone_url,
    }
}

func convert_as_gitlab_vars(vars map[string]string) *[]*gitlab.PipelineVariableOptions {
    var ret []*gitlab.PipelineVariableOptions
    for k,v := range vars {
        ret = append(ret, &gitlab.PipelineVariableOptions{Key: gitlab.String(k), Value: gitlab.String(v)})
    }
    return &ret
}

type schedulerData struct {
    dbData ScheduleDataNoVars
    job *gocron.Job
}
