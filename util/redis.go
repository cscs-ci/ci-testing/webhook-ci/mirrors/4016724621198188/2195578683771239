package util

import (
    "context"
    "encoding/json"
    "fmt"
    "time"

    "github.com/redis/go-redis/v9"
    "github.com/rs/zerolog"
    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
)

type Redis struct {
    client *redis.Client
}
var redis_context = context.Background()


func GetRedis(config RedisConfig) *Redis {
    logging.Debugf("Connecting to redis at %v", config.Address)
    ret := Redis{}
    ret.client = redis.NewClient(&redis.Options{
        Addr: config.Address,
        Password: config.Password,
        DB: 0,
    })
    if _, err := ret.client.Ping(redis_context).Result(); err != nil {
        logging.Errorf(err, "Failed pinging redis. err=%v", err)
    }
    return &ret
}


func (r *Redis) Close() error {
    return r.client.Close()
}

func (r *Redis) GetJobFromToken(token string, job_id int, ctx context.Context, glserver *gitlab.Client, logger *zerolog.Logger) (*gitlab.Job, error) {
    if logger == nil {
        logger = logging.Get()
    }
    if data, err := r.client.Get(ctx, token).Bytes(); err == nil {
        logger.Debug().Msgf("Found data for token %v in redis cache", token)
        var ret gitlab.Job
        if err := json.Unmarshal([]byte(data), &ret); err != nil {
            logger.Warn().Msgf("Found entry for token=%v, but data could not be unmarshalled. Fetching fresh from gitlab", token)
        } else {
            if ret.ID == job_id {
                logger.Debug().Msgf("Hit redis cache for token=%v", token)
                return &ret,nil
            } else {
                logger.Warn().Msgf("Found entry for token=%v, but job_id did not match. job_id in cache=%v, requested job_id=%v. Fetching fresh from gitlab", token, ret.ID, job_id)
            }
        }
    }

    jobTokenOptions := &gitlab.GetJobTokensJobOptions{JobToken: gitlab.String(token)}
    var ret *gitlab.Job
    var err error
    if ! r.IsJobCanceled(int64(job_id)) {
        ret, _, err = glserver.Jobs.GetJobTokensJob(jobTokenOptions, GitlabRequestMustSucceed(ctx))
    } else {
        ret, _, err = glserver.Jobs.GetJobTokensJob(jobTokenOptions)
    }
    if err == nil {
        if job_id == 0 {
            // job_id was not present in the query URL
            job_id = ret.ID
        }
        if ret.ID != job_id {
            err = fmt.Errorf("job_id=%v does not match job id obtained via token", job_id)
            logger.Error().Err(err).Msg(err.Error())
            return nil, err
        }

        // cache token result
        if data, err := json.Marshal(*ret); err != nil {
            logger.Warn().Msgf("Failed marshalling gitlab.Job to store in redis cache. err=%v", err)
        } else {
            logger.Debug().Msgf("Storing job data to redis cache for token=%v", token)
            if _, err := r.client.SetEx(ctx, token, data, 24*time.Hour).Result(); err != nil {
                logger.Warn().Msgf("Failed adding data to redis cache. err=%v", err)
            }
            err = r.StoreJobInfo(int64(job_id), map[string]any{"token": token})
        }
    }
    return ret, err
}

func (r *Redis) StoreJobInfo(job_id int64, data map[string]any) error {
    key := fmt.Sprintf("job:%v", job_id)
    if _, err := r.client.HSet(redis_context, key, data).Result(); err != nil {
        return err
    }
    if _, err := r.client.Expire(redis_context, key, 24*time.Hour).Result(); err != nil {
        return err
    }
    return nil
}

func (r *Redis) GetJobInfo(job_id int64) (map[string]string, error) {
    key := fmt.Sprintf("job:%v", job_id)
    return r.client.HGetAll(redis_context, key).Result()
}

func (r *Redis) SetJobCanceled(job_id int64, is_canceled bool) error {
    return r.StoreJobInfo(job_id, map[string]any{"is_canceled": is_canceled})
}
func (r *Redis) IsJobCanceled(job_id int64) bool {
    key := fmt.Sprintf("job:%v", job_id)
    if is_canceled, err := r.client.HGet(redis_context, key, "is_canceled").Bool(); err != nil {
        if err != redis.Nil {
            logging.Errorf(err, "Failed redis.HGet for `is_canceled` and job_id=%v", job_id)
        }
        return false
    } else {
        return is_canceled
    }
}

func (r *Redis) StoreTriggerStatus(reqId string, data map[string]string) error {
    key := fmt.Sprintf("trigger:%v", reqId)
    if _, err := r.client.HSet(redis_context, key, data).Result(); err != nil {
        return err
    }
    if _, err := r.client.Expire(redis_context, key, 24*time.Hour).Result(); err != nil {
        return err
    }
    return nil
}

func (r *Redis) GetTriggerStatus(reqId string) (map[string]string, error) {
    key := fmt.Sprintf("trigger:%v", reqId)
    return r.client.HGetAll(redis_context, key).Result()
}
