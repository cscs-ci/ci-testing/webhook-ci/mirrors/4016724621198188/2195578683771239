package util

import (
    "errors"
    "fmt"
    "net/url"
    "strings"
    "time"


    "golang.org/x/exp/slices"
    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
)

// see for pipeline/job statuses here (they have the same values, but we observe only a subset of them)
// https://docs.gitlab.com/ee/api/graphql/reference/#pipelinestatusenum
// https://docs.gitlab.com/ee/api/graphql/reference/#cijobstatus
// CANCELED                 A job that is canceled.
// CREATED                  A job that is created.
// FAILED                   A job that is failed.
// MANUAL                   A job that is manual.
// PENDING                  A job that is pending.
// PREPARING                A job that is preparing.
// RUNNING                  A job that is running.
// SCHEDULED                A job that is scheduled.
// SKIPPED                  A job that is skipped.
// SUCCESS                  A job that is success.
// WAITING_FOR_RESOURCE     A job that is waiting for resource.
const GlStatusCanceled = "canceled"
const GlStatusCanceling = "canceling"
const GlStatusCreated = "created"
const GlStatusFailed = "failed"
const GlStatusManual = "manual"
const GlStatusPending = "pending"
const GlStatusPreparing = "preparing"
const GlStatusRunning = "running"
const GlStatusScheduled = "scheduled"
const GlStatusSkipped = "skipped"
const GlStatusSuccess = "success"
const GlStatusWaiting = "waiting_for_resource"

var githubStatusMap = map[string]string {
    GlStatusRunning: "pending",
    GlStatusCreated: "pending",
    GlStatusFailed: "failure",
    GlStatusCanceled: "error",
    GlStatusCanceling: "error",
}
var bitbucketStatusMap = map[string]string {
    GlStatusRunning: "INPROGRESS",
    GlStatusCreated: "INPROGRESS",
    GlStatusPending: "INPROGRESS",
    GlStatusFailed: "FAILED",
    GlStatusSuccess: "SUCCESSFUL",
    GlStatusCanceled: "STOPPED",
    GlStatusCanceling: "STOPPED",
}
var repoStatusMap = map[RepoType]map[string]string {
    Github: githubStatusMap,
    Bitbucket: bitbucketStatusMap,
    Gitlab: {},
    UnknownRepo: {},
}

type RepoType int
const (
    UnknownRepo RepoType = iota
    Github
    Gitlab
    Bitbucket
)

func GetRepoType(repo_url string) RepoType {
    if strings.HasPrefix(repo_url, "https://github.com") { return Github }
    if strings.HasPrefix(repo_url, "https://api.github.com") { return Github }
    if strings.HasPrefix(repo_url, "https://gitlab.com") { return Gitlab }
    if strings.HasPrefix(repo_url, "https://bitbucket.org") { return Bitbucket }
    if strings.HasPrefix(repo_url, "https://api.bitbucket.org") { return Bitbucket }
    if strings.HasPrefix(repo_url, "https://git.cscs.ch") { return Gitlab }
    if strings.HasPrefix(repo_url, "https://gitlab.jsc.fz-juelich.de") { return Gitlab }
    if strings.HasPrefix(repo_url, "https://gitlab.dkrz.de") { return Gitlab }
    if strings.HasPrefix(repo_url, "https://git.iac.ethz.ch") { return Gitlab }
    return UnknownRepo
}


func GetNotificationUrl(repo_url string, sha string) string {
    logging.Debugf("GetNotificationUrl for repo_url=%v and sha=%v", repo_url, sha)

    var commit_status_url string
    switch GetRepoType(repo_url) {
    case Github:
        // we need to transfrom https://github.com/namespace/project --> https://api.github.com/repos/namespace/project/statuses/{sha}
        commit_status_url = fmt.Sprintf("%v/statuses/%v", strings.Replace(repo_url, "https://github.com", "https://api.github.com/repos", 1), sha)
    case Gitlab:
        // we need to transform https://gitlab.com/namespace/project --> https://gitlab.com/api/v4/projects/namespace%2Fproject/statuses/{sha}
        if u, err := url.Parse(repo_url); err != nil {
            logging.Errorf(err, "Could not parse url=%v", repo_url)
        } else {
            // u.Path starts with '/', i.e. we strip the first character
            commit_status_url = fmt.Sprintf("%v://%v/api/v4/projects/%v/statuses/%v", u.Scheme, u.Host, url.PathEscape(u.Path[1:]), sha)
        }
    case Bitbucket:
        // we need to transform https://bitbucket.org/namespace/project --> https://api.bitbucket.org/2.0/repositories/namespace/project/commit/{sha}/statuses/build
        commit_status_url = fmt.Sprintf("%v/commit/%v/statuses/build", strings.Replace(repo_url, "https://bitbucket.org", "https://api.bitbucket.org/2.0/repositories", 1), sha)
    }
    return commit_status_url
}

func NotifyRepo(url, gitlab_status, target_url, name, notification_token, description string, ref string) (*ResponseHelper, error) {
    logging.Debugf("Notifying url=%v gitlab_status=%v target_url=%v name=%v description=%v", url, gitlab_status, target_url, name, description)

    // do not process pipeline when it is skipped
    if gitlab_status == GlStatusSkipped {
        return nil,nil
    }
    // do not send notification if requested to discard notification
    if name == "__discard-notification" {
        return nil, nil
    }

    repo_type := GetRepoType(url)
    status_name := "cscs/" + name
    status := gitlab_status
    if repoStatusMap[repo_type][gitlab_status] != "" {
        status = repoStatusMap[repo_type][gitlab_status]
    }

    var bodydata map[string]string
    var headers = map[string]string{}

    switch repo_type {
    case Github:
        headers = map[string]string{"Authorization": fmt.Sprintf("Bearer %v", notification_token),
                                    "X-GitHub-Api-Version": "2022-11-28"}
        bodydata = map[string]string{"state": status,
                                     "target_url": target_url,
                                     "description": description,
                                     "context": status_name}
    case Bitbucket:
        headers = map[string]string{"Authorization": fmt.Sprintf("Bearer %v",notification_token)}
        bodydata = map[string]string{"state": status,
                                     "url": target_url,
                                     "description": description,
                                     "key": status_name}
        if ref != "" && strings.HasPrefix(ref, "__CSCSCI__pr") == false {
            bodydata["refname"] = ref
        }
    case Gitlab:
        headers = map[string]string{"Authorization": fmt.Sprintf("Bearer %v", notification_token)}
        bodydata = map[string]string{"state": status,
                                     "context": status_name,
                                     "target_url": target_url,
                                     "description": description}
        if ref != "" && strings.HasPrefix(ref, "__CSCSCI__pr") == false {
            bodydata["ref"] = ref
        }
    default:
        return nil, errors.New("Unknown repo provided")
    }

    if resp, err := DoJsonRequest("POST", url, headers, bodydata); err != nil {
        return nil, err
    } else {
        return resp, nil
    }
}


type BackgroundNotifier struct {
    glserver *gitlab.Client
    db DB
    config Config
}

func NewBackgroundNotifier(glserver *gitlab.Client, db DB, config Config) BackgroundNotifier {
    return BackgroundNotifier{glserver, db, config}
}

func (bn BackgroundNotifier) Watch() {
    // we want to avoid that a pipeline/job handler notifies while we also start our loop to avoid double notification
    // Therefore we remember which repos would need a notification and only notify when we see it a second time
    sunSeen := make(map[StatusUpdateNeeded]bool)
    for {
        if all_status_updates_needed, err := bn.db.GetAllStatusUpdateNeeded(); err != nil {
            logging.Error(err, "Failed getting all status_update_needed rows from database")
        } else {
            for _, sun := range all_status_updates_needed {
                repository_id := bn.db.GetRepositoryId(sun.PipelineID)
                notification_token := bn.db.GetNotificationToken(repository_id)
                glProjectPath := fmt.Sprintf("%v/%v/%v", bn.config.Gitlab.MirrorsPath, repository_id, sun.PipelineID)
                pipelines, _, err := bn.glserver.Pipelines.ListProjectPipelines(glProjectPath, &gitlab.ListProjectPipelinesOptions{SHA: gitlab.String(sun.SHA)})
                if err != nil {
                    logging.Errorf(err, "Failed getting pipelines for SHA=%v", sun.SHA)
                    continue
                }
                var pipeline *gitlab.PipelineInfo
                for _, p := range pipelines {
                    if strings.ToLower(p.Status) != GlStatusSkipped {
                        pipeline = p
                        break
                    }
                }
                if pipeline == nil {
                    logging.Warnf("Found 0 pipelines with SHA=%v", sun.SHA)
                    continue
                }
                if pipeline.Status == GlStatusRunning || pipeline.Status == GlStatusPending {
                    // do not notify when running. We have already sent a running notification, because the only way that something is in the database
                    // is when we receive a pipeline hook that states that a pipeline is created/running
                    continue
                }
                if _, ok := sunSeen[sun]; ok {
                    logging.Infof("Notifying for pipeline_id=%v target_url=%v sha=%v name=%v status=%v", sun.PipelineID, sun.TargetURL, sun.SHA, sun.Name, pipeline.Status)
                    commit_url := GetNotificationUrl(bn.db.GetRepoUrl(repository_id), pipeline.SHA)
                    if slices.Contains([]string{GlStatusSuccess, GlStatusFailed, GlStatusCanceled, GlStatusSkipped}, pipeline.Status) {
                        if err := bn.db.DelStatusUpdateNeeded(sun.PipelineID, sun.TargetURL,sun.SHA, sun.Name); err != nil {
                            logging.Error(err, "Failed deleting StatusUpdateNeeded from database")
                        }
                    }
                    if notification_token == "" {
                        logging.Infof("NotifyRepo at url=%v, but notification_token is empty. Not trying to notify", commit_url)
                    } else {
                        resp, err := NotifyRepo(commit_url, pipeline.Status, sun.TargetURL, sun.Name, notification_token, "", pipeline.Ref)
                        if err != nil {
                            logging.Error(err, "Failed notifying original repository")
                            continue
                        }
                        if resp != nil {
                            if err := CheckResponse(resp); err != nil {
                                logging.Error(err, "Failed notifying original repository")
                                continue
                            }
                        }
                    }
                    delete(sunSeen, sun)
                } else {
                    sunSeen[sun] = true
                }
            }
        }
        time.Sleep(600 * time.Second)
    }
}
