import os

from typing import TypedDict

import mariadb
import yaml


# only types that we actually are using. The config has more fields, but they are not mentioned here
class DbConfigType(TypedDict):
    user: str
    password: str
    address: str
    dbname: str
class GitlabConfigType(TypedDict):
    url: str
    mirrors_path: str
    token: str
    container_builder_service_project: int
class JFrogConfigType(TypedDict):
    url: str
    token: str
    username: str
    email: str
class ConfigType(TypedDict):
    db: DbConfigType
    gitlab: GitlabConfigType
    jfrog: JFrogConfigType
    registry_path: str


class Config(object):
    def __init__(self, config_path: str):
        with open(config_path) as f:
            self._cfg: ConfigType = yaml.safe_load(f)
        if 'CIEXT_JFROG_TOKEN' in os.environ:
            self._cfg['jfrog']['token'] = os.environ['CIEXT_JFROG_TOKEN']
        if 'CIEXT_GITLAB_TOKEN' in os.environ:
            self._cfg['gitlab']['token'] = os.environ['CIEXT_GITLAB_TOKEN']
        if 'CIEXT_DATABASE_PASSWORD' in os.environ:
            self._cfg['db']['password'] = os.environ['CIEXT_DATABASE_PASSWORD']

        db_addr = self._cfg['db']['address'].split(':')
        if len(db_addr) > 2:
            raise RuntimeError(f'Failed parsing config db.address to be of the form hostname[:port]')
        self._dbConn = mariadb.connect(user=self._cfg['db']['user'],
                                       password=self._cfg['db']['password'],
                                       host=db_addr[0],
                                       port=int(db_addr[1]) if len(db_addr)==2 else 3306,
                                       database=self._cfg['db']['dbname']
       )

    def getDbCursor(self) -> mariadb.Cursor:
        return self._dbConn.cursor()

    def jfrogUrl(self) -> str:
        return self._cfg['jfrog']['url']
    def jfrogUsername(self) -> str:
        return self._cfg['jfrog']['username']
    def jfrogPassword(self) -> str:
        return self._cfg['jfrog']['token']
    def jfrogRepoDir(self) -> str:
        return '/'.join(self._cfg['registry_path'].split('/')[1:])

