import argparse
import re
from typing import TypedDict, List

import yaml
import requests

import config

class JFrogUser(TypedDict):
    username: str
    uri: str
    realm: str
    status: str
class JFrogGetUsersResponse(TypedDict):
    users: List[JFrogUser]
    cursor: str

class JFrogPermission(TypedDict):
    name: str
    uri: str
class JFrogGetPermissionsResponse(TypedDict):
    permissions: List[JFrogPermission]
    cursor: str


jfrog_user_re = re.compile('ci-ext-[0-9]+')
jfrog_permission_re = re.compile('ci-ext-[0-9]+-[0-9]+')

def is_ciext_jfrog_username(x: JFrogUser) -> bool:
    return jfrog_user_re.fullmatch( x['username']) != None

def is_ciext_jfrog_permission(x: JFrogPermission) -> bool:
    return jfrog_permission_re.fullmatch(x['name']) != None

def delete_user_in_jfrog(x: JFrogUser) -> None:
    r = requests.delete(f'{cfg.jfrogUrl()}/access/api/v2/users/{x["username"]}', headers={'Authorization': f'Bearer {cfg.jfrogPassword()}'}, timeout=60)
    r.raise_for_status()

def delete_permission_in_jfrog(x: JFrogPermission) -> None:
    r = requests.delete(f'{cfg.jfrogUrl()}/access/api/v2/permissions/{x["name"]}', headers={'Authorization': f'Bearer {cfg.jfrogPassword()}'}, timeout=60)
    r.raise_for_status()

def get_all_db_access_token_usernames(cfg: config.Config) -> set[str]:
    cur = cfg.getDbCursor()
    cur.execute('select username from access_tokens')
    return set([x for (x,) in cur])

def get_ciext_jfrog_users(cfg: config.Config) -> List[JFrogUser]:
    r = requests.get(f'{cfg.jfrogUrl()}/access/api/v2/users?limit=10000', headers={'Authorization': f'Bearer {cfg.jfrogPassword()}'}, timeout=60)
    r.raise_for_status()
    respData: JFrogGetUsersResponse = r.json()
    return [ x for x in respData['users'] if is_ciext_jfrog_username(x) ]

def get_ciext_jfrog_permissions(cfg: config.Config) -> List[JFrogPermission]:
    r = requests.get(f'{cfg.jfrogUrl()}/access/api/v2/permissions?limit=10000', headers={'Authorization': f'Bearer {cfg.jfrogPassword()}'}, timeout=60)
    r.raise_for_status()
    respData: JFrogGetPermissionsResponse = r.json()
    return [ x for x in respData['permissions'] if is_ciext_jfrog_permission(x) ]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', required=True)
    args = parser.parse_args()
    cfg = config.Config(args.config)

    # first get all usernames/permissions from jfrog
    jfrog_users = get_ciext_jfrog_users(cfg)
    jfrog_permissions = get_ciext_jfrog_permissions(cfg)

    # now get all tokens from database that should exist
    all_db_access_token_usernames = get_all_db_access_token_usernames(cfg)

    print(f'{jfrog_users=}')
    print(f'{jfrog_permissions=}')
    print(f'{all_db_access_token_usernames=}')

    # delete all usernames / permissions in jfrog that are not known in database, they must be leftovers and not been deleted correctly
    for user in jfrog_users:
        if user['username'] not in all_db_access_token_usernames:
            print(f'Deleting user in JFrog with username={user["username"]}, because it is not known in the access_token database table')
            delete_user_in_jfrog(user)
        else:
            print(f'Not deleting user in JFrog with username={user["username"]}, because it is known in the access_token database table')

    for permission in jfrog_permissions:
        if permission['name'] not in all_db_access_token_usernames:
            print(f'Deleting permission in JFrog with name={permission["name"]}, because it is now known in the access_token database table')
            delete_permission_in_jfrog(permission)
        else:
            print(f'Not deleting permission in JFrog with name={permission["name"]}, because it is known in the access_token database table')
