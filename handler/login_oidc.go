package handler

import (
    "context"
    "net/http"
    "net/http/httputil"
    "net/url"
    "strings"

    "github.com/snapcore/snapd/randutil"
    "github.com/xanzy/go-gitlab"
    "golang.org/x/oauth2"
    "github.com/golang-jwt/jwt/v5"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

type debugTransport struct {
}
func (dt debugTransport) RoundTrip(r *http.Request) (*http.Response, error) {
    reqData, err := httputil.DumpRequest(r, true)
    if err != nil {
        logging.Error(err, "Failed dumping request")
    } else {
        logging.Warnf("Request dump: %v", string(reqData))
    }

    resp, err := http.DefaultTransport.RoundTrip(r)
    if resp != nil {
        respData, err := httputil.DumpResponse(resp, true)
        if err != nil {
            logging.Error(err, "Failed dumping response")
        } else {
            logging.Warnf("Response dump: %v", string(respData))
        }
    }
    return resp, err
}

func GetOidcLoginHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(oidcLoginHandler{glserver, db, config})
}

type oidcLoginHandler struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}

func (o oidcLoginHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    redirect_query := r.URL.Query().Get("redirect")
    if redirect_query == "" {
        redirect_query = util.BuildCIURL("/overview", &o.config)
    }

    if strings.HasSuffix(r.URL.Path, "/auth") {
        // first check if we are already authorized - we enforce an online check, i.e. KC must validate the token.
        if authorized, _ := is_authorized(r, w, true); authorized {
            // we are already authorized, and the session was validated online - no need to go through the full OAuth2 workflow
            http.Redirect(w, r, redirect_query, http.StatusFound)
            return
        }
        // Redirect user to consent page to ask for permission
        // for the scopes specified above.
        verifier := oauth2.GenerateVerifier()
        nonce, err := randutil.CryptoToken(16)
        pie(logger.Error, err, "Failed creating random token", http.StatusInternalServerError)
        url := oauth2_config.AuthCodeURL(nonce, oauth2.AccessTypeOnline, oauth2.SetAuthURLParam("claims", `{"id_token":{"groups":{"essential":true}}}`), oauth2.S256ChallengeOption(verifier))
        session := GetSessionStore().GetSession(r, nonce)
        session.Values["redirect"] = redirect_query
        session.Values["verifier"] = verifier
        session.Options.MaxAge = 600 // Maximum time of 10 minutes to finalize login procedure
        if err := session.Save(r, w); err != nil {
            logger.Error().Err(err).Msg("Failing storing session")
        }
        http.Redirect(w, r, url, http.StatusFound)
    } else if strings.HasSuffix(r.URL.Path, "callback") {
        ctx := context.Background()
        // debug requests/responses from the client
        //my_debug_transport := &debugTransport{}
        //my_http_client := &http.Client{Transport: my_debug_transport}
        //ctx = context.WithValue(ctx, oauth2.HTTPClient, my_http_client)
        code := r.URL.Query().Get("code")
        if code == "" {
            logger.Error().Msgf("No code received from authorization server. URL=%v", r.URL)
            pie(logger.Error, condition_error{"No code received from authorization server"}, "", http.StatusBadRequest)
        }
        state := r.URL.Query().Get("state")
        if state == "" {
            logger.Error().Msgf("No state received from authorization server. URL=%v", r.URL)
            pie(logger.Error, condition_error{"No state received from authorization server"}, "", http.StatusBadRequest)
        }
        session := GetSessionStore().GetSession(r, state)
        verifier, exists := session.Values["verifier"]
        if !exists {
            _, _ = w.Write([]byte("Authentication timed out"))
            return
        }
        tok, err := oauth2_config.Exchange(ctx, code, oauth2.VerifierOption(verifier.(string)))
        pie(logger.Error, err, "Failed retrieving token", http.StatusInternalServerError)

        // test token
        _, err = oauth2_config.Client(ctx, tok).Get(userinfo_endpoint)
        pie(logger.Error, err, "Failed getting userinfo", http.StatusInternalServerError)

        sessionAuth := GetSessionStore().GetSession(r, sessionAuth)
        sessionAuth.Values[sessionAuthTokenKey] = *tok
        sessionAuth.Values[sessionAuthIdTokenKey] = tok.Extra("id_token")
        if err := sessionAuth.Save(r, w); err != nil {
            logger.Error().Err(err).Msg("Failed storing session")
        }
        if access_token, err := jwt.Parse(tok.AccessToken, jwks_keyfunc.Keyfunc); err != nil {
            logger.Error().Err(err).Msg("Failed parsing JWT.")
        } else {
            sid := access_token.Claims.(jwt.MapClaims)["sid"].(string)
            session_state := r.URL.Query().Get("session_state")
            if session_state != sid {
                // This can probably be removed, the reason is to trace if sid != session_state at any time.
                logger.Warn().Msgf("sid and session_state do not match. sid=%v, session_state=%v", sid, session_state)
            }
            o.db.SaveJwtSessionId(sessionAuth.ID, sid)
        }
        http.Redirect(w, r, session.Values["redirect"].(string), http.StatusFound)
    } else {
        http.Error(w, "Bad request", http.StatusBadRequest)
    }
}

func forward_to_auth(w http.ResponseWriter, r *http.Request, cfg *util.Config) {
    redirect_to := util.BuildCIURL(r.URL.String(), cfg)
    auth_url := util.BuildCIURL("/oauth2/auth?redirect=%v", cfg, url.PathEscape(redirect_to))
    http.Redirect(w, r, auth_url, http.StatusFound)
}
