package handler;

import (
    "encoding/json"
    "io"
    "net/http"
    "strconv"

    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetRedisJobHandler(glserver *gitlab.Client, db util.DB, config util.Config, redis *util.Redis) func(w http.ResponseWriter, r *http.Request) {
    return wrap(redisJob{glserver, db, config, redis})
}

type redisJob struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
    redis *util.Redis
}

func (h redisJob) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    job_id, err := strconv.Atoi(r.URL.Query().Get("job_id"))
    if err != nil {
        logger.Warn().Msgf("Failed parsing job_id=%v as integer. err=%v. token=%v", r.URL.Query().Get("job_id"), err, r.URL.Query().Get("token"))
    }

    job, err := h.redis.GetJobFromToken(r.URL.Query().Get("token"), job_id, r.Context(), h.glserver, logger)
    pie(logger.Warn, err, "Could not fetch token's job", http.StatusBadRequest)

    incoming_data := map[string]any{}
    incoming_bytes, err := io.ReadAll(r.Body)
    pie(logger.Error, err, "Failed reading body from request", http.StatusInternalServerError)
    err = json.Unmarshal(incoming_bytes, &incoming_data)
    pie(logger.Error,err, "Failed parsing body as json object", http.StatusBadRequest)

    err = h.redis.StoreJobInfo(int64(job.ID), incoming_data)
    pie(logger.Error, err, "Failed storing data in redis cache", http.StatusInternalServerError)
}
