package handler

import (
    "net/http"
    "sync"

    "github.com/gorilla/securecookie"
    "github.com/gorilla/sessions"
    "github.com/srinathgs/mysqlstore"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

var sessionsOnce sync.Once
type SessionStore struct {
    *mysqlstore.MySQLStore
}
var sessionStore SessionStore
var cookie_secret_key = "my-secret-key"
var cookie_path = "/"
var cookie_max_age = 3600

const sessionAuth = "ciext_auth_session"
const sessionAuthTokenKey = "token"
const sessionAuthUserinfoKey = "userinfo"
const sessionAuthIdTokenKey = "id_token"


func SetSessionStoreParams(secret, path string, max_age int) {
    cookie_secret_key = secret
    cookie_path = path
    cookie_max_age = max_age
}

func GetSessionStore() *SessionStore {
    sessionsOnce.Do(func() {
        var err error
        if sessionStore.MySQLStore, err = mysqlstore.NewMySQLStoreFromConnection(util.GetDbRawConnection(), "websessions", cookie_path, cookie_max_age, []byte(cookie_secret_key)); err != nil {
            logging.Error(err, "Error creating websessions store")
            panic(err)
        }
        sessionStore.Codecs[0].(*securecookie.SecureCookie).MaxLength(1048576)
    })
    return &sessionStore
}

func (s *SessionStore) GetSession(r *http.Request, session_name string) *sessions.Session {
    if session, err := sessionStore.Get(r, session_name); err != nil {
        logging.Errorf(err, "Failed retrieving web session, err=%v", err)
        session, _ := sessionStore.New(r, "")
        return session
    } else {
        return session
    }
}
