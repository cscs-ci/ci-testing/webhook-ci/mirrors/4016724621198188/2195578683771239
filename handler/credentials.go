package handler

import (
    "encoding/json"
    "fmt"
    "net/http"
    "regexp"
    "strconv"
    "strings"

    "github.com/xanzy/go-gitlab"
    "github.com/rs/zerolog"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetCredentialsHandler(glserverJobToken *gitlab.Client, db util.DB, config util.Config, redis *util.Redis) func(w http.ResponseWriter, r *http.Request) {
    return wrap(credentialsHandler{glserver: glserverJobToken, db: db, config: config, redis: redis})
}

type credentialsHandler struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
    redis *util.Redis
    reqLogger *zerolog.Logger
}

type credential struct {
    // credentials are either username+password, or a key
    Username string `json:"username,omitempty"`
    Password string `json:"password,omitempty"`
    Key string      `json:"key,omitempty"`
}

func (h credentialsHandler) Get(w http.ResponseWriter, r* http.Request) {
    logger := logging.GetReqLogger(r)
    h.reqLogger = logger

    job_id, err := strconv.Atoi(r.URL.Query().Get("job_id"))
    if err != nil {
        logger.Warn().Msgf("Failed parsing job_id=%v as integer. err=%v. token=%v", r.URL.Query().Get("job_id"), err, r.URL.Query().Get("token"))
    }

    job, err := h.redis.GetJobFromToken(r.URL.Query().Get("token"), job_id, r.Context(), h.glserver, logger)
    pie(logger.Warn, err, "Could not fetch token's job", http.StatusBadRequest)

    logger.Debug().Msgf("Job token=%v belongs to job_id=%v at %v", r.URL.Query().Get("token"), job.ID, job.WebURL)

    reg_exp := fmt.Sprintf("^%s/%s/([0-9]+)/([0-9]+)/.*/%d$", h.config.Gitlab.Url, h.config.Gitlab.MirrorsPath, job.ID)

    re := regexp.MustCompile(reg_exp)
    matches := re.FindStringSubmatch(job.WebURL)

    if len(matches) != 3 {
        pie(logger.Error, condition_error{Err: "Job's web_url did not match the expected format"},
            fmt.Sprintf("web_url=%s did not match the expected format=%s", job.WebURL, reg_exp), http.StatusBadRequest)
    }

    repository_id, err := strconv.ParseInt(matches[1], 10, 64)
    pie(logger.Error, err, "Job's web url did not match the expected format", http.StatusBadRequest)
    pipeline_id, err := strconv.ParseInt(matches[2], 10, 64)
    pie(logger.Error, err, "Job's web url did not match the expected format", http.StatusBadRequest)

    if repository_id != h.db.GetRepositoryId(pipeline_id) {
        pie(logger.Error, condition_error{Err: "repository id in database and web url do not match"}, "Job's web_url did not match the expected format", http.StatusBadRequest)
    }

    ret := map[string]credential{}
    for _, cred_id := range strings.Split(r.URL.Query().Get("creds"), ",") {
        switch cred_id {
        case "container_registry":
            ret["container_registry"] = h.get_container_registry_creds(job, repository_id)
        case "spack_buildcache_storage":
            ret["spack_buildcache_storage"] = h.get_spack_buildcache_storage_creds(job, repository_id)
        case "spack_sign_key":
            spack_signkey, err := h.config.GetSpackSignKey()
            pie(logger.Error, err, "Failed getting spack sign key", http.StatusInternalServerError)
            ret["spack_sign_key"] = credential{Key: string(spack_signkey)}
        case "sshkey":
            sshkey, err := h.config.GetSshKey(repository_id, false, false)
            pie(logger.Error, err, "Failed getting sshkey", http.StatusInternalServerError)
            ret["sshkey"] = credential{Key: string(sshkey)}
        case "f7t":
            client_id := h.db.GetF7tClientId(repository_id)
            client_secret := h.db.GetF7tClientSecret(repository_id)
            ret["f7t"] = credential{Username: client_id, Password: client_secret}
        default:
            pie(logger.Error, condition_error{Err: fmt.Sprintf("Request for credential %s could not be satisfied", cred_id)}, "Access denied", http.StatusBadRequest)
        }
    }
    w.Header().Add("Content-Type", "application/json")
    logger.Debug().Msgf("Sending credentials %v", ret)
    if err := json.NewEncoder(w).Encode(ret); err != nil {
        logger.Error().Err(err).Msg("Failed encoding msg to json")
    }
}


func (h *credentialsHandler) jfrog_create_access_tokens(job *gitlab.Job, repository_id int64) (string, string, error) {
    username := fmt.Sprintf("ci-ext-%v", job.ID)
    password, err := util.CreateJFrogUser(username, h.config.JFrog.EMail, h.config.JFrog.URL, h.config.JFrog.Token)
    pie(h.reqLogger.Error, err, "Failed creating new username in JFrog", http.StatusInternalServerError)
    h.reqLogger.Debug().Msgf("Successfully created new user in Jrog with name=%v", username)
    err = h.db.AddCredential(job.Pipeline.ProjectID, job.ID, username, password, util.SystemJFrogUser)
    pie(h.reqLogger.Error, err, "Failed adding credential to DB", http.StatusInternalServerError)

    registryAllowPaths, err := h.db.GetRegistryAllowPaths(repository_id)
    pie(h.reqLogger.Error, err, "Failed getting allowed registry paths from database", http.StatusInternalServerError)
    permission_index := 0
    for repo, include_patterns := range registryAllowPaths {
        permissionName := fmt.Sprintf("%v-%v", username, permission_index)
        err = util.CreateJFrogPermission(permissionName, username, repo, include_patterns, h.config.JFrog.URL, h.config.JFrog.Token)
        pie(h.reqLogger.Error, err, "Failed creating permission", http.StatusInternalServerError)
        err = h.db.AddCredential(job.Pipeline.ProjectID, job.ID, permissionName, "", fmt.Sprintf("%v.%v", util.SystemJFrogPerm, permission_index))
        pie(h.reqLogger.Error, err, "Failed adding credential to DB", http.StatusInternalServerError)
        h.reqLogger.Debug().Msgf("Successfully created new permission in jfrog with name=%v", permissionName)
        permission_index += 1
    }

    return username, password, nil
}

func (h *credentialsHandler) get_container_registry_creds(job *gitlab.Job, repository_id int64) credential {
    if username, password, found, err := h.db.GetCredential(job.ID, util.SystemJFrogUser); err != nil {
        pie(h.reqLogger.Error, err, "Failed querying database for access_tokens", http.StatusInternalServerError)
    } else if found {
        return credential{Username: username, Password: password}
    }

    username, password, err := h.jfrog_create_access_tokens(job, repository_id)
    pie(h.reqLogger.Error, err, "Failed creating new user in jfrog", http.StatusInternalServerError)
    h.reqLogger.Debug().Msgf("Created a new user in JFrog username=%v, password=%v for job=%v", username, password, job.ID)

    return credential{Username: username, Password: password}
}

func (h *credentialsHandler) get_spack_buildcache_storage_creds(job *gitlab.Job, repository_id int64) credential {
    if username, password, found, err := h.db.GetCredential(job.ID, util.SystemCastor); err != nil {
        pie(h.reqLogger.Error, err, "Failed querying database for access_tokens", http.StatusInternalServerError)
    } else if found {
        return credential{Username: username, Password: password}
    }

    username, password, err := util.CreateCastorEC2Credentials(h.config.SpackBuildcache.Cloud)
    pie(h.reqLogger.Error, err, "Failed creating ec2 credentials in castor", http.StatusInternalServerError)
    err = h.db.AddCredential(job.Pipeline.ProjectID, job.ID, username, password, util.SystemCastor)
    pie(h.reqLogger.Error, err, "Failed adding credential to database", http.StatusInternalServerError)

    return credential{Username: username, Password: password}
}
