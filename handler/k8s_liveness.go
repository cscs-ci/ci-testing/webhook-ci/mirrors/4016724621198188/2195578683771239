package handler

import (
    "context"
    "net/http"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)


func GetK8sLivenessHandler(db util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(k8sLivenessHandler{DB: db})
}

type k8sLivenessHandler struct {
    DB util.DB
}

func (h k8sLivenessHandler) Get(w http.ResponseWriter, r* http.Request) {
    logger := logging.GetReqLogger(r)
    *r = *r.WithContext(context.WithValue(r.Context(), logging.ContextLogReqAsDebugKey, true))
    if h.DB.GetRepoUrl(123456) == "" {
        pie(logger.Error, db_access_error{Err: "Failed getting RepoUrl for project with id=123456"}, "Liveness for DB access failed", http.StatusInternalServerError)
    }
    _, _ = w.Write([]byte("ok"))
}
