package handler

import (
    "context"
    "errors"
    "fmt"
    "io/ioutil"
    "net/http"
    "strconv"
    "time"

    "github.com/rs/zerolog"
    "golang.org/x/oauth2"
    "github.com/golang-jwt/jwt/v5"
    "github.com/MicahParks/keyfunc/v2"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

type access_error struct {}
func (e access_error) Error() string {
    return "Access error"
}

type condition_error struct {
    Err string
}
func (e condition_error) Error() string {
    return "Some expected condition was not met. Err=" + e.Err
}

type db_access_error struct {
    Err string
}
func (e db_access_error) Error() string {
    return "There was an error accessing the database. Err=" + e.Err
}

type pipeline_not_found_error struct {
    PipelineName string
}
func (e pipeline_not_found_error) Error() string {
    return "Could not find pipeline with name=" + e.PipelineName
}


// panic-if-error
func pie(logFct func()*zerolog.Event, err error, msg string, statuscode int) {
    if err != nil {
        if msg == "" {
            msg = err.Error()
        }
        logFct().Err(err).Msg(msg)
        // logging/request_logger will recover from this panic, log it and write to the http.ResponseWriter
        panic(logging.NewHandlerError(err, msg, statuscode))
    }
}

var jwks_keyfunc *keyfunc.JWKS = nil
var userinfo_endpoint = ""
var oauth2_config *oauth2.Config = nil
func PrepareJwksKeyfunc(url string, userinfo_ep string) {
    userinfo_endpoint = userinfo_ep
    options := keyfunc.Options {
        RefreshInterval: time.Hour*24,
        RefreshRateLimit: time.Minute*1,
        RefreshUnknownKID: true,
    }

    var err error
    if jwks_keyfunc, err = keyfunc.Get(url, options); err != nil {
        panic(fmt.Sprintf("Error while fetching JWKS certificates. err=%v", err))
    }
}
func PrepareOauth2Config(client_id, client_secret, auth_url, token_url, redirect_url string) {
    oauth2_config = &oauth2.Config {
        ClientID:     client_id,
        ClientSecret: client_secret,
        Scopes:       []string{"openid", "profile", "email"},
        RedirectURL: redirect_url,
        Endpoint: oauth2.Endpoint {
            AuthURL:  auth_url,
            TokenURL: token_url,
        },
    }
}

// Returns true if the user is authorized at all, i.e. the user successfully logged in through keycloak
// enforce_online - If true, then irrespective of the validity of a JWT, the /userinfo endpoint will be contacted
//                  otherwise a user is considered authorized if the JWT has not expired yet, if it is expired it
//                  will do an online validation, i.e. same as enforce_online=true
// returns as second argument a list of claims, i.e. the username and the groups the user belongs to
func is_authorized(r* http.Request, w http.ResponseWriter, enforce_online bool) (bool, []string) {
    session := GetSessionStore().GetSession(r, sessionAuth)
    if token, exists := session.Values[sessionAuthTokenKey]; exists {
        tok := token.(oauth2.Token)
        // the documention of jwt.Parse suggest to use WithValidMethods() as parser option, however keyfunc implicitly is doing this.
        // From the documentation at: https://pkg.go.dev/github.com/MicahParks/keyfunc/v2#section-readme
        // This package will check the alg in each JWK. If present, it will confirm the same alg is in a given JWT's header before returning the key for signature verification.
        // If the algs do not match, keyfunc.ErrJWKAlgMismatch will prevent the key being used for signature verification. If the alg is not present in the JWK, this check will not occur.
        access_token, err := jwt.Parse(tok.AccessToken, jwks_keyfunc.Keyfunc)

        if err != nil {
            if errors.Is(err, jwt.ErrTokenExpired) {
                ctx := context.Background()
                if token_new, err := oauth2_config.TokenSource(ctx, &tok).Token(); err != nil {
                    // session already expired on the oauth2 provider side, and cannot be refreshed anymore
                    logging.Debugf("'Failed getting token from tokensource, i.e. token could not be refreshed. err=%v", err)
                    return false, nil
                } else {
                    // token was refreshed, save the refreshed token in the session
                    // this is an online action, i.e. enforce_online is implicitly fulfilled
                    if *token_new==tok {
                        logging.Warn("Token before and after refresh is the same and did not change")
                    }
                    session := GetSessionStore().GetSession(r, sessionAuth)
                    session.Values[sessionAuthTokenKey] = *token_new
                    session.Values[sessionAuthIdTokenKey] = token_new.Extra("id_token")
                    if err := session.Save(r, w); err != nil {
                        logging.Error(err, "Failed storing session")
                    }
                    // although err != nil, access_token has been parsed and can be used to get the claims
                    return true, authorized_claims(access_token)
                }
            } else {
                logging.Errorf(err, "Failed parsing JWT")
                return false, nil
            }
        } // err != nil

        if enforce_online == false && access_token.Valid {
            return true, authorized_claims(access_token)
        }
        if enforce_online {
            ctx := context.Background()
            client := oauth2_config.Client(ctx, &tok)
            if resp, err := client.Get(userinfo_endpoint); err != nil {
                logging.Error(err, "Failed calling userinfo endpoint")
                return false, nil
            } else {
                if resp.StatusCode >= 400 {
                    respBody, _ := ioutil.ReadAll(resp.Body)
                    logging.Warnf("userinfo endpoint returned statuscode=%v, which indicates an error, body=%v", resp.StatusCode, string(respBody))
                    return false, nil
                }
                return true, authorized_claims(access_token)
            }
        }
    }

    return false, nil
}

// returns a list where the first entry is the username this token belongs to
// and the rest are the groups the user is part of
func authorized_claims(token *jwt.Token) []string {
    var ret []string
    mapclaims := token.Claims.(jwt.MapClaims)
    if username, exists := mapclaims["preferred_username"]; exists == false {
        logging.Errorf(condition_error{"JWT claims did not include the username"}, "JWT claims did not include the username. All claims=%#v", mapclaims)
    } else {
        ret = append(ret, username.(string))
        for _, g := range mapclaims["groups"].([]any) {
            // group names are of the form `/my_group`, hence we skip the first character.
            ret = append(ret, g.(string)[1:])
        }
    }
    return ret
}

// returns -1 if not authorized - no error is returned
func authorized_for_repo(r* http.Request) int64 {
    user, pass, ok := r.BasicAuth()
    // no BasicAuth header --> not authorized for any repo
    if ok == false {
        return -1
    }

    // username cannot be converted to a string --> not authorized to any repo
    repo_id, err := strconv.ParseInt(user, 10, 64)
    if err != nil {
        return -1
    }

    // repo_id and webhook_secret match --> authorized for this repo_id
    if util.GetDb().GetWebhookSecret(repo_id) == pass {
        return repo_id
    }
    // repo_id and firecrest secret match --> authorized for this repo_id
    if util.GetDb().GetF7tClientSecret(repo_id) != "" && util.GetDb().GetF7tClientSecret(repo_id) == pass {
        return repo_id
    }

    // default is that we are not authorized to any repo
    return -1
}
