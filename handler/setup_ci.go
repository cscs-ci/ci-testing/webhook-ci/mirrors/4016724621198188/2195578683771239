package handler

import (
    "crypto/sha256"
    "encoding/base64"
    "encoding/json"
    "fmt"
    "net/http"
    "os"
    "regexp"
    "strconv"
    "strings"

    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetSetupCIHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(setupCIHandler{glserver, db, config})
}

type setupCIHandler struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}


func (h setupCIHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    var err error
    var repo_id int64 = -1

    if repo_id, err = strconv.ParseInt(r.URL.Query().Get("repo"), 10, 64); err != nil || h.db.GetRepositoryName(repo_id) == "" {
        // invalid repo id in URL - forward to overview site
        http.Redirect(w, r, util.BuildCIURL("/overview", &h.config), http.StatusFound)
        return
    }

    if authorized, claims := is_authorized(r, w, false); authorized {
        // we are authorized - check if we have admin rights
        if h.db.GetAccessLevelForRepoWithClaims(repo_id, claims) < util.AdminAccess {
            http.Error(w, "You are not an administrator of this repository. Access denied", http.StatusForbidden)
            return
        }
    } else {
        forward_to_auth(w, r, &h.config)
        return
    }

    // special case, where we want to test if a notification token is a valid token (requested from frontend)
    if repo_id != -1 && r.URL.Query().Get("test_notification_token") != "" {
        _, _ = w.Write(h.testNotificationToken(repo_id, r.URL.Query().Get("test_notification_token")))
        return
    }

    selected_pipeline := r.URL.Query().Get("pipeline")
    pipeline_ids := h.db.GetPipelines(repo_id)
    var pipelines []pipelineTmplData
    for _, pipeline_id := range pipeline_ids {
        schedules := []pipelineScheduleTmplData{}
        schedule_data := h.db.GetPipelineSchedules(pipeline_id)
        for _, schedule := range schedule_data {
            cron_schedule := fmt.Sprintf("%v %v", schedule.CronSchedule, schedule.Ref)
            for key,value := range schedule.Variables {
                cron_schedule += fmt.Sprintf(";%v=%v", key, value)
            }
            schedules = append(schedules, pipelineScheduleTmplData{schedule.ScheduleID, cron_schedule})
        }
        if len(schedules) == 0 {
            schedules = []pipelineScheduleTmplData{{-1, ""}}
        }
        pipelines = append(pipelines, pipelineTmplData{
            ID: pipeline_id,
            CIEntrypoint: h.db.GetCIEntrypoint(pipeline_id),
            Name: h.db.GetPipelineName(pipeline_id),
            TrustedUser: strings.Join(h.db.GetGitUsersPipeline(pipeline_id), ","),
            CIBranches: strings.Join(h.db.GetBranchesPipeline(pipeline_id), ","),
            Schedule: schedules,
            TriggerPR: h.db.GetTriggerPR(pipeline_id),
            Variables: h.db.GetPipelineVariables(pipeline_id),
        })
    }

    repo_url := h.db.GetRepoUrl(repo_id)
    repo_owner := h.db.GetRepoOwner(repo_id)
    repo_admins := h.db.GetRepoAdmins(repo_id)
    repo_managers := h.db.GetRepoManagers(repo_id)
    repo_type := util.GetRepoType(repo_url)
    repo_f7t_client_id := h.db.GetF7tClientId(repo_id)
    repo_f7t_slurm_account := h.db.GetF7tSlurmAccount(repo_id)
    webhook_url := util.BuildCIURL("/webhook_ci?id=%v", &h.config, repo_id)
    var repo_webhook_url, repo_webhook_setup, repo_provider string
    switch repo_type {
    case util.Github:
        repo_provider = "github"
        repo_webhook_url = fmt.Sprintf("%v/settings/hooks", repo_url)
        repo_webhook_setup = fmt.Sprintf("Payload URL: %v", webhook_url)
        repo_webhook_setup += "\nContent type: application/json"
        repo_webhook_setup += "\nSecret: The webhook secret that was sent to you by the CSCS-CI administrator"
        repo_webhook_setup += "\nWhich events would you like to trigger this webhook: Send me everything"
    case util.Bitbucket:
        repo_provider = "bitbucket"
        repo_webhook_url = fmt.Sprintf("%v/admin/webhooks", repo_url)
        repo_webhook_setup = "Title: Choose any name (e.g. CSCS-CI)"
        repo_webhook_setup += fmt.Sprintf("\nURL: %v&secret=REPLACE_WITH_WEBHOOK_SECRET", webhook_url)
        repo_webhook_setup += "\nTriggers: Tick all checkboxes (some features might not work correctly if you skip some triggers)"
    case util.Gitlab:
        repo_provider = "gitlab"
        repo_webhook_url = fmt.Sprintf("%v/-/hooks", repo_url)
        repo_webhook_setup = fmt.Sprintf("URL: %v", webhook_url)
        repo_webhook_setup += "\nSecret token: The webhook secret that was sent to you by the CSCS-CI administrator"
        repo_webhook_setup += "\nTrigger: Tick all checkboxes (some features might not work correctly if you skip some triggers)"
    }

    // nosemgrep: gosec.G304-1
    ssh_public_key, err := os.ReadFile(h.config.GetSSHKeyPath(repo_id, false, true))
    pie(logger.Error, err, "Failed reading SSH public key", http.StatusInternalServerError)
    pkey, err := base64.StdEncoding.DecodeString(strings.Split(string(ssh_public_key), " ")[1])
    pie(logger.Error, err, "Failed base64 decoding the public key", http.StatusInternalServerError)
    pkey_sha256 := sha256.Sum256(pkey)
    ssh_public_key_fingerprint := "SHA256:" + base64.RawStdEncoding.EncodeToString(pkey_sha256[:])

    tmpl_data := setupCITmplData{
        RepoName: h.db.GetRepositoryName(repo_id),
        RepoID: repo_id,
        RepoURL: repo_url,
        RepoOwner: repo_owner,
        RepoAdmins: strings.Join(repo_admins, ","),
        RepoManagers: strings.Join(repo_managers, ","),
        RepoF7tClientId: repo_f7t_client_id,
        RepoF7tSlurmAccount: repo_f7t_slurm_account,
        IsPrivate: h.db.IsPrivateRepo(repo_id),
        TrustedUser: strings.Join(h.db.GetGitUsersDefault(repo_id), ","),
        CIBranches: strings.Join(h.db.GetBranchesDefault(repo_id), ","),
        Pipelines: pipelines,
        SelectedPipeline: selected_pipeline,
        UrlPrefix: h.config.URLPrefix,
        RepoProvider: repo_provider,
        RepoWebhookUrl: repo_webhook_url,
        RepoWebhookSetup : repo_webhook_setup,
        SSHKeyPublic: string(ssh_public_key),
        SSHKeyPublicFingerprint: ssh_public_key_fingerprint,
        Favicon: "setup-favicon.svg",
        Variables: h.db.GetRepositoryVariables(repo_id),
    }

    err = util.ExecuteTemplate(w, "setup_ci.html", &tmpl_data)
    pie(logger.Error, err, "Error rendering template setup_ci.html", http.StatusInternalServerError)
}

type setupCITmplData struct {
    RepoName string
    RepoID int64
    RepoURL string
    RepoOwner string
    RepoAdmins string
    RepoManagers string
    RepoF7tClientId string
    RepoF7tSlurmAccount string
    IsPrivate bool
    TrustedUser string
    CIBranches string
    Pipelines []pipelineTmplData
    SelectedPipeline string
    UrlPrefix string
    RepoProvider string
    RepoWebhookUrl string
    RepoWebhookSetup string
    SSHKeyPublic string
    SSHKeyPublicFingerprint string
    Favicon string
    Variables []util.Variable
}

type pipelineTmplData struct {
    ID int64
    CIEntrypoint string
    Name string
    TrustedUser string
    CIBranches string
    Schedule []pipelineScheduleTmplData
    TriggerPR bool
    Variables []util.Variable
}

type pipelineScheduleTmplData struct {
    ScheduleID int64
    CronSchedule string
}

// WARNING: This function does not validate any authorization anymore. Any security checks MUST
// be done before calling save_setup()
func save_setup(r *http.Request, db util.DB, glserver *gitlab.Client, config util.Config) int64 {
    logger := logging.GetReqLogger(r)
    err := r.ParseForm()
    pie(logger.Error, err, "Failed parsing form", http.StatusBadRequest)
    if r.URL.Query().Get("repo") != r.FormValue("repository_id") {
        pie(logger.Error, condition_error{"id in query and form do not match"}, "", http.StatusBadRequest)
    }
    repo_id, err := strconv.ParseInt(r.FormValue("repository_id"), 10, 64)
    pie(logger.Error, err, "Failed parsing repository_id as an integer", http.StatusBadRequest)

    repo_name := r.FormValue("name")
    private_repo := r.FormValue("privaterepo") != ""
    notification_token := r.FormValue("notification_token")
    trusted_user := r.FormValue("trusted_user")
    ci_branches := r.FormValue("ci_branches")
    admin_user := r.FormValue("admin_user")
    manager_user := r.FormValue("manager_user")
    f7t_client_id := r.FormValue("f7t_client_id")
    f7t_client_secret := r.FormValue("f7t_client_secret")
    f7t_slurm_account := r.FormValue("f7t_slurm_account")

    if  notification_token != "" {
        db.UpdateNotificationToken(repo_id, notification_token)
    }

    db.UpdateF7tClientId(repo_id, f7t_client_id)
    if f7t_client_secret != "" {
        db.UpdateF7tClientSecret(repo_id, f7t_client_secret)
    }
    db.UpdateF7tSlurmAccount(repo_id, f7t_slurm_account)

    // global variables
    idx := 0
    for {
        if val, exists := r.Form[fmt.Sprintf("global_var_key_%v", idx)]; exists {
            if len(val) > 1 {
                logger.Warn().Msgf("Received for global_var_key_%v more than one value. Values=%v - this is unexpected and only the first will be considered", idx, val)
            }
            key := r.FormValue(fmt.Sprintf("global_var_key_%v", idx))
            if r.FormValue(fmt.Sprintf("global_var_status_%v", idx)) == "deleted" {
                db.DeleteRepositoryVariable(repo_id, key)
            } else if r.FormValue(fmt.Sprintf("global_var_status_%v", idx)) == "new" {
                value := r.FormValue(fmt.Sprintf("global_var_value_%d", idx))
                secret := r.FormValue(fmt.Sprintf("global_var_secret_%d", idx)) == "true"
                key_valid, err := regexp.MatchString("^[a-zA-Z0-9_]*$", key)
                pie(logger.Error, err, "Could not validate variable key", http.StatusInternalServerError)
                if key != "" && key_valid {
                    db.AddRepositoryVariable(repo_id, key, value, secret)
                }
            }
            idx++
        } else {
            // no more variables
            break
        }
    }

    // we split at comma, but also trim any whitespace
    my_split := func(in string) []string {
        splitted := strings.Split(in, ",")
        trimmed := util.SliceMap(splitted, func(x string)string{return strings.TrimSpace(x)})
        return util.SliceFilter(trimmed, func(x string) bool { return x!="" })
    }
    _ = db.ReplaceGitUsersDefault(repo_id, my_split(trusted_user))
    _ = db.ReplaceBranchesDefault(repo_id, my_split(ci_branches))
    _ = db.ReplaceRepoAdmins(repo_id, my_split(admin_user))
    _ = db.ReplaceRepoManagers(repo_id, my_split(manager_user))

    name_changed := db.GetRepositoryName(repo_id) != repo_name
    private_changed := private_repo != db.IsPrivateRepo(repo_id)
    visibility := gitlab.PrivateVisibility
    if !private_repo {
        visibility = gitlab.PublicVisibility
    }

    // if new visibility is public we must first make sure that the group becomes public,
    // only then projects can be set to public visibility.
    // The opposite applies when we want to make it private, then we have to update
    // first all projects to visibility=private beore updating the group
    updateGroup := func() {
        if name_changed || private_changed {
            logger.Info().Msg("Name or Private flag changed. Updating Group on gitlab")
            db.UpdateRepositoryName(repo_id, repo_name)
            db.UpdatePrivateRepoFlag(repo_id, private_repo)
            newName := fmt.Sprintf("%v-%v", repo_name, repo_id)
            updates := &gitlab.UpdateGroupOptions{
                Name: &newName,
                Visibility: &visibility,
            }
            if _, _, err := glserver.Groups.UpdateGroup(fmt.Sprintf("%v/%v", config.Gitlab.MirrorsPath, repo_id), updates); err != nil {
                logger.Error().Err(err).Msg("Failed updating group for mirrors")
            }
        }
    }
    if visibility == gitlab.PublicVisibility {
        updateGroup()
    }

    pipeline_ids := db.GetPipelines(repo_id)
    for _, pipeline_id := range pipeline_ids {
        pipeline_name := r.FormValue(fmt.Sprintf("%v_name", pipeline_id))
        if matched, err := regexp.MatchString("^[a-zA-Z0-9\\-]+$", pipeline_name); err != nil || matched == false {
            this_error := condition_error{fmt.Sprintf("Invalid pipeline name %v", pipeline_name)}
            pie(logger.Error, this_error, "", http.StatusBadRequest)
        }
        entrypoint := r.FormValue(fmt.Sprintf("%v_entrypoint", pipeline_id))
        trusted_user = r.FormValue(fmt.Sprintf("%v_trusted_user", pipeline_id))
        ci_branches = r.FormValue(fmt.Sprintf("%v_ci_branches", pipeline_id))
        trigger_pr := r.FormValue(fmt.Sprintf("%v_trigger_pr", pipeline_id)) != ""
        if err := db.ReplaceGitUsersPipeline(pipeline_id, my_split(trusted_user)); err != nil {
            logger.Error().Err(err).Msg("Failed ReplaceGitUsersPipeline in database")
        }
        if err := db.ReplaceBranchesPipeline(pipeline_id, my_split(ci_branches)); err != nil {
            logger.Error().Err(err).Msg("Failed ReplaceBranchesPipeline in database")
        }
        db.UpdateTriggerPRFlag(pipeline_id, trigger_pr)
        for _, schedule := range db.GetPipelineSchedules(pipeline_id) {
            new_cron_schedule := r.FormValue(fmt.Sprintf("%v_schedule_%v", pipeline_id, schedule.ScheduleID))
            if new_cron_schedule == "" {
                db.DeletePipelineSchedule(schedule.ScheduleID)
            } else if new_cron_schedule != schedule.CronSchedule {
                if matched, err := regexp.MatchString("^(|[0-9]+ ([0-9,*\\/\\-]+ ){4}.+)$", new_cron_schedule); err != nil || matched == false {
                    this_error := condition_error{fmt.Sprintf("The cron schedule %v is invalid", new_cron_schedule)}
                    pie(logger.Error, this_error, "", http.StatusBadRequest)
                }
                cron_time, ref, variables := parse_schedule_line(new_cron_schedule)
                db.UpdatePipelineSchedule(schedule.ScheduleID, cron_time, ref, variables)
            }
        }
        // pipeline variables
        idx := 0
        for {
            if val, exists := r.Form[fmt.Sprintf("%v_var_key_%v",pipeline_id, idx)]; exists {
                if len(val) > 1 {
                    logger.Warn().Msgf("Received for %v_var_key_%v more than one value. Values=%v - this is unexpected and only the first will be considered", pipeline_id, idx, val)
                }
                key := r.FormValue(fmt.Sprintf("%v_var_key_%v", pipeline_id, idx))
                if r.FormValue(fmt.Sprintf("%v_var_status_%v", pipeline_id, idx)) == "deleted" {
                    db.DeletePipelineVariable(pipeline_id, key)
                } else if r.FormValue(fmt.Sprintf("%v_var_status_%v", pipeline_id, idx)) == "new" {
                    value := r.FormValue(fmt.Sprintf("%v_var_value_%d", pipeline_id, idx))
                    secret := r.FormValue(fmt.Sprintf("%v_var_secret_%d", pipeline_id, idx)) == "true"
                    key_valid, err := regexp.MatchString("^[a-zA-Z0-9_]*$", key)
                    pie(logger.Error, err, "Could not validate variable key", http.StatusInternalServerError)
                    if key != "" && key_valid {
                        db.AddPipelineVariable(pipeline_id, key, value, secret)
                    }
                }
                idx++
            } else {
                // no more variables
                break
            }
        }
        new_schedule_idx := -1
        for {
            new_cron_schedule := r.FormValue(fmt.Sprintf("%v_schedule_%v", pipeline_id, new_schedule_idx))
            if new_cron_schedule == "" {
                break
            } else {
                if matched, err := regexp.MatchString("^(|[0-9]+ ([0-9,*\\/\\-]+ ){4}.+)$", new_cron_schedule); err != nil || matched == false {
                    this_error := condition_error{fmt.Sprintf("The cron schedule %v is invalid", new_cron_schedule)}
                    pie(logger.Error, this_error, "", http.StatusBadRequest)
                }
                cron_time, ref, variables := parse_schedule_line(new_cron_schedule)
                db.AddPipelineSchedule(pipeline_id, cron_time, ref, variables)
                new_schedule_idx -= 1
            }
        }
        pipeline_name_changed := pipeline_name != db.GetPipelineName(pipeline_id)
        entrypoint_changed := entrypoint != db.GetCIEntrypoint(pipeline_id)
        if pipeline_name_changed || name_changed || entrypoint_changed || private_changed {
            db.UpdatePipelineName(pipeline_id, pipeline_name)
            db.UpdateCIEntrypoint(pipeline_id, entrypoint)
            newProjectName := fmt.Sprintf("%v-%v", repo_name, pipeline_name)
            edits := &gitlab.EditProjectOptions{
                Name: &newProjectName,
                CIConfigPath: &entrypoint,
                Visibility: &visibility,
            }
            // it can happen that the gitlab API call fails. E.g. a new pipeline is added and renamed. Normally the mirror is not created on the gitlab side at this point
            // because it would only be created the first time we see a need to actually create it. Hence we log it only as a warning, but normally this is fine and (sometimes) expected
            _, glresp, err := glserver.Projects.EditProject(fmt.Sprintf("%v/%v/%v", config.Gitlab.MirrorsPath, repo_id, pipeline_id), edits)
            if err != nil {
                logger.Warn().Msgf("error when trying to update project details. err=%v, glresp.StatusCode=%v. This can happen when the mirror has not yet been created.", err, glresp.StatusCode)
            }
            //pie(logger.Error, err, "Failed editing pipeline mirror", glresp.StatusCode)
        }
    }

    if visibility == gitlab.PrivateVisibility {
        updateGroup()
    }

    return repo_id
}


func (h setupCIHandler) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    var err error
    var repo_id int64 = -1

    if repo_id, err = strconv.ParseInt(r.URL.Query().Get("repo"), 10, 64); err != nil || h.db.GetRepositoryName(repo_id) == "" {
        // invalid repo id in URL - forward to overview site
        http.Error(w, "repo parameter is missing or invalid", http.StatusBadRequest)
        return
    }

    if authorized, claims := is_authorized(r, w, false); authorized {
        // we are authorized - check if we have admin rights
        if h.db.GetAccessLevelForRepoWithClaims(repo_id, claims) < util.AdminAccess {
            http.Error(w, "You are not an administrator of this repository. Access denied", http.StatusForbidden)
            return
        }
    } else {
        http.Error(w, "You are not authorized. The session has expired. No data was saved.", http.StatusForbidden)
        return
    }

    save_setup(r, h.db, h.glserver, h.config)

    redirect := util.BuildCIURL("/setup/ui?repo=%v", &h.config, repo_id)
    if r.URL.Query().Get("action") == "create" {
        new_pipeline_id, err := h.db.AddPipeline(repo_id, "unnamed")
        pie(logger.Error, err, "Error adding new pipeline", http.StatusInternalServerError)
        http.Redirect(w, r, fmt.Sprintf("%v&pipeline=%v", redirect, new_pipeline_id), http.StatusFound)
    } else {
        if r.URL.Query().Get("action") == "delete" {
            pipeline_id, err := strconv.ParseInt(r.URL.Query().Get("pipeline_id"), 10, 64)
            pie(logger.Error, err, "Failed parsing pipeline_id as integer", http.StatusBadRequest)
            // it can be that the project was never created, i.e. there is nothing to delete
            if _, err := h.glserver.Projects.DeleteProject(fmt.Sprintf("%v/%v/%v", h.config.Gitlab.MirrorsPath, repo_id, pipeline_id)); err != nil {
                logger.Error().Err(err).Msg("Failed deleting mirror repository")
            }
            ssh_keys := [2]string{h.config.GetSSHKeyPath(pipeline_id, true, false), h.config.GetSSHKeyPath(pipeline_id, true, true)}
            for _, key_path := range ssh_keys {
                if err := os.Remove(key_path); err != nil {
                    logger.Error().Err(err).Msgf("Failed removing file %v", key_path)
                }
            }
            if _, err := h.db.DeletePipeline(pipeline_id); err != nil {
                logger.Error().Err(err).Msg("Failed deleting pipeline from database")
            }
        }
        http.Redirect(w, r, redirect, http.StatusFound)
    }
}

func (h *setupCIHandler) testNotificationToken(repo_id int64, notification_token string) []byte {
    if notification_token == "__dbvalue" {
        notification_token = h.db.GetNotificationToken(repo_id)
        if notification_token == "" {
            return []byte("Notification token is not set. Please set a valid notification token")
        }
    }
    repo_url := h.db.GetRepoUrl(repo_id)
    notify_url := util.GetNotificationUrl(repo_url, "0000000000000000000000000000000000000000")
    resp, err := util.NotifyRepo(notify_url, util.GlStatusSuccess, util.BuildCIURL("/setup/ui?repo=%v", &h.config, repo_id), "notify_test", notification_token, "Test notification token", "")
    if err != nil {
        logging.Errorf(err, "Error while trying to test notification token for repo_id=%v", repo_id)
        return []byte(err.Error())
    }
    responseData := make(map[string]any)
    if err := json.Unmarshal(resp.ResponseData, &responseData); err != nil {
        logging.Errorf(err, "Failed unmarshalling data from JSON. responseData=%v", string(resp.ResponseData))
        return []byte(err.Error())
    }
    if util.GetRepoType(repo_url) == util.Github {
        if resp.StatusCode != 422 || responseData["message"] != "No commit found for SHA: 0000000000000000000000000000000000000000" {
            return []byte("Notification token is invalid")
        }

    } else if util.GetRepoType(repo_url) == util.Bitbucket {
        if resp.StatusCode != 200 || responseData["key"] != "cscs/notify_test" {
            return []byte("Notification token is invalid")
        }
    } else if util.GetRepoType(repo_url) == util.Gitlab {
        if resp.StatusCode != 404 || responseData["message"] != "404 Commit Not Found" {
            return []byte("Notification token is invalid")
        }
    }
    return []byte("ok")
}


func parse_schedule_line(cron_schedule_line string) (string, string, map[string]string) {
    split := strings.Split(cron_schedule_line, " ")
    cron_time := strings.Join(split[0:5], " ")
    ref_variables := strings.Split(strings.TrimSpace(split[5]), ";")
    ref := ref_variables[0]
    variables := make(map[string]string)
    for _, variable := range ref_variables[1:] {
        var_split := strings.SplitN(variable, "=", 2)
        variables[var_split[0]] = var_split[1]
    }
    return cron_time, ref, variables
}
