package handler

import (
    "fmt"
    "net/http"
    "sort"
    "strings"
    
    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

type overview struct{
    config *util.Config
    db *util.DB
}
func (h overview) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    if authorized, claims := is_authorized(r, w, false); authorized {
        access_for_repos := h.db.GetRepositoryAccessForClaims(claims)
        var admin_projects []overviewRepoTmplData
        var manager_projects []overviewRepoTmplData
        for _, access := range access_for_repos {
            repoTmplData := overviewRepoTmplData {
                SetupUrl : fmt.Sprintf("setup/ui?repo=%v", access.RepoID),
                Name : h.db.GetRepositoryName(access.RepoID),
                RepoUrl: h.db.GetRepoUrl(access.RepoID),
            }
            if access.AccessLevel >= util.AdminAccess {
                admin_projects = append(admin_projects, repoTmplData)
            } else if access.AccessLevel >= util.ManagerAccess {
                manager_projects = append(manager_projects, repoTmplData)
            }
        }

        sort.Slice(admin_projects, func(i, j int) bool { return strings.ToLower(admin_projects[i].Name) < strings.ToLower(admin_projects[j].Name); })
        sort.Slice(manager_projects, func(i, j int) bool { return strings.ToLower(manager_projects[i].Name) < strings.ToLower(manager_projects[j].Name); })
        can_register, _ := can_register_project(w, r, h.config)
        tmpl_data := overviewTmplData {
            UrlPrefix: h.config.URLPrefix,
            AdminProjects: admin_projects,
            ManagerProjects: manager_projects,
            CanRegister: can_register,
        }
        err := util.ExecuteTemplate(w, "overview.html", &tmpl_data)
        pie(logger.Error, err, "Error rendering template overview.html", http.StatusInternalServerError)
    } else {
        forward_to_auth(w, r, h.config)
        return
    }
}

func GetOverviewHandler(config *util.Config, db *util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(overview{config: config, db: db})
}

type overviewTmplData struct {
    UrlPrefix string
    Favicon string
    AdminProjects []overviewRepoTmplData
    ManagerProjects []overviewRepoTmplData
    CanRegister bool
}
type overviewRepoTmplData struct {
    SetupUrl string
    Name string
    RepoUrl string
}
