package handler

import (
    "encoding/json"
    "fmt"
    "net/http"
    "net"
    "os/exec"
    "strings"
    "time"
    "github.com/go-git/go-git/v5"

    gitconfig "github.com/go-git/go-git/v5/config"
    "github.com/go-git/go-git/v5/plumbing/object"
    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetUenvBuildHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(uenvBuild{glserver, db, config})
}

type uenvBuild struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}

// body must be the Dockerfile to build - will redirect to a website, which has the results
func (h uenvBuild) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    //token := strings.TrimSpace(strings.Replace(r.Header.Get("Authorization"), "Bearer", "", 1))
    //scopes, err := validate_jwt(token)
    //pie(logger.Error, err, "Authorization failed", http.StatusUnauthorized)

    //if util.SliceContains(scopes, "ciext") == false {
    //    logger.Warn().Msg("Could not find scope 'ciext' in JWT. This is not yet a hard error.")
    //}

    // allow calling this endpoint only within CSCS network
    ip_addr := r.Header.Get("X-Forwarded-For")
    parsed_ip := net.ParseIP(ip_addr)
    if parsed_ip == nil {
        msg := "You are not allowed to use the uenv build service"
        logger.Debug().Msgf("parsed_ip is nil, therefore the uenv build service is blocked. X-Original-Forwarded-For=%v X-Forwarded-For=%v",
            r.Header.Get("X-Original-Forwarded-For"),
            r.Header.Get("X-Forwarded-For"))
        pie(logger.Error, condition_error{msg}, "", http.StatusBadRequest)
    }
    // check if it is in a valid subnet
    _, subnet1, _ := net.ParseCIDR("148.187.0.0/16")
    _, subnet2, _ := net.ParseCIDR("172.16.0.0/12")
    if false == subnet1.Contains(parsed_ip) && false == subnet2.Contains(parsed_ip) {
        msg := "You are not allowed to use the uenv build service"
        logger.Debug().Msgf("parsed_ip is %v, but is not within the allowed subnets, therefore the uenv build service is blocked", parsed_ip)
        pie(logger.Error, condition_error{msg}, "", http.StatusBadRequest)
    }

    pipeline_id := h.config.Gitlab.UenvBuilderServicePipelineId
    repo_id := h.db.GetRepositoryId(pipeline_id)
    new_branch := fmt.Sprintf("%v-%v", logging.GetReqCorrelationID(r), pipeline_id)
    git_target_path := fmt.Sprintf("/home/tmp/uenv_build/%v", new_branch)

    for _, param := range []string{"system", "name", "version", "uarch"} {
        if r.URL.Query().Get(param) == "" {
            msg := fmt.Sprintf("Query parameter '%v' is missing", param)
            pie(logger.Error, condition_error{msg}, "", http.StatusBadRequest)
        }
    }
    variables := map[string]string{}
    for k, v := range r.URL.Query() {
        variables[k] = strings.Join(v, ",")
    }

    util.LockRepoDir(git_target_path, logger)
    defer util.UnlockRepoDir(git_target_path, logger)

    repo, err := util.GitCloneOrOpen(&h.db, &h.config, git_target_path, repo_id, false, logger, nil)
    pie(logger.Error, err, "Failed clonging/opening the uenv build project", http.StatusInternalServerError)

    err = util.GitFetch(&h.db, &h.config, repo, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/main:refs/heads/main")}, repo_id, logger, nil)
    pie(logger.Error, err, "Failed fetching main branch for uenv build project", http.StatusInternalServerError)

    err = util.EnsureMirrorRepo(&h.db, &h.config, h.glserver, logger, repo_id, pipeline_id)
    pie(logger.Error, err, "Failed creating mirror repository", http.StatusInternalServerError)

    worktree, err := repo.Worktree()
    pie(logger.Error, err, "Failed opening worktree from repo", http.StatusInternalServerError)

    // extract
    tarCmd := exec.Command("tar",
        "-xzf", "-",
        "--directory="+git_target_path+"/recipe") // #nosec G204 - golang escapes arguments, no injection possible
    tarCmd.Stdout = w
    tarCmd.Stderr = w
    tarCmd.Stdin = r.Body
    err = tarCmd.Run()
    pie(logger.Error, err, "Failed extracting build context", http.StatusBadRequest)

    // add new files/directories
    status, err := worktree.Status()
    pie(logger.Error, err, "Failed getting status of repository", http.StatusInternalServerError)
    for filename, filestatus := range status {
        if filestatus.Worktree == git.Untracked || filestatus.Worktree == git.Modified {
            _, err := worktree.Add(filename)
            pie(logger.Error, err, "Failed adding file to build context", http.StatusInternalServerError)
        }
    }

    commit, err := worktree.Commit("update", &git.CommitOptions{
        Author: &object.Signature{Name: "CSCS CI-Ext", Email: "ciext-admin@cscs.ch", When: time.Now()},
    })
    pie(logger.Error, err, "failed commiting to worktree", http.StatusInternalServerError)

    refspecs := []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+refs/heads/main:refs/heads/%v", new_branch))}
    err = util.GitPush(&h.db, &h.config, repo, refspecs, repo_id, pipeline_id, logger, nil)
    pie(logger.Error, err, "Failed pushing changes to mirror repository", http.StatusInternalServerError)

    var triggered_pipeline_id int
    triggered_pipeline_id, err = util.TriggerPipelineWithRetry(h.glserver, &h.config, &h.db, pipeline_id, new_branch, commit.String(), variables, logger)
    pie(logger.Error, err, fmt.Sprintf("Failed triggering pipeline with err=%v", err), http.StatusBadRequest)

    w.Header().Add("Content-Type", "application/json")

    tag := fmt.Sprintf("%v", triggered_pipeline_id)
    if tag_in_vars, ok := variables["UENV_TAG"]; ok {
        tag = tag_in_vars
    }

    ret := uenvBuildReturn {
        BuildLogURL: util.BuildCIURL("/%v?image=%v", &h.config, r.URL.Path, new_branch),
        Status: "submitted",
        Destination: uenvBuildDestination {
            Registry: fmt.Sprintf("%v/uenv", strings.Split(h.config.RegistryPath, "/")[0]),
            Namespace: "service",
            Label: fmt.Sprintf("%v/%v:%v@%v%%%v", variables["name"], variables["version"], tag, variables["system"], variables["uarch"]),
        },
    }

    if err := json.NewEncoder(w).Encode(ret); err != nil {
        logger.Error().Err(err).Msg("Failed encoding msg to json")
    }
}

func (h uenvBuild) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    branch_name := r.URL.Query().Get("image")
    if branch_name == "" {
        pie(logger.Error, condition_error{"Parameter image is empty"}, "", http.StatusBadRequest)
    }
    pipeline_id := h.config.Gitlab.UenvBuilderServicePipelineId
    repo_id := h.db.GetRepositoryId(pipeline_id)
    mirror_project := fmt.Sprintf("%v/%v/%v", h.config.Gitlab.MirrorsPath, repo_id, pipeline_id)
    pipelines, _, err := h.glserver.Pipelines.ListProjectPipelines(mirror_project, &gitlab.ListProjectPipelinesOptions{Ref: &branch_name})
    pie(logger.Error, err, "Failed getting pipeline", http.StatusBadRequest)
    var pipeline *gitlab.PipelineInfo
    for _, p := range pipelines {
        if p.Status != util.GlStatusSkipped {
            pipeline = p
        }
    }
    if pipeline == nil {
        pie(logger.Error, condition_error{"Could not find pipeline with this name"}, "", http.StatusBadRequest)
    }

    jobs, _, err := h.glserver.Jobs.ListPipelineJobs(mirror_project, pipeline.ID, &gitlab.ListJobsOptions{IncludeRetried: gitlab.Bool(false)})
    pie(logger.Error, err, "Failed getting pipeline's jobs", http.StatusBadRequest)

    redirect_to := util.BuildCIURL("/job/result/%v/%v/%v", &h.config, pipeline_id, pipeline.ProjectID, jobs[0].ID)
    http.Redirect(w, r, redirect_to, http.StatusFound)
}

type uenvBuildDestination struct {
    Registry string `json:"registry"`
    Namespace string `json:"namespace"`
    Label string `json:"label"`
}
type uenvBuildReturn struct {
    BuildLogURL string `json:"build_log_url"`
    Status string `json:"status"`
    Destination uenvBuildDestination `json:"destination"`
}
