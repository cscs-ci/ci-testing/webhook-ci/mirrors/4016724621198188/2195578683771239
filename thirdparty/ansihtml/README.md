# ansihtml

> Go package to parse ANSI escape sequences to HTML.

## Important note
This package was copied from https://github.com/robert-nix/ansihtml/ and modified to fit the needed usecase

## Usage

```go
ansihtml.ConvertToHTML(io.Reader readEnd, io.Writer writeEnd)
html := new(bytes.Buffer)
ansihtml.ConvertToHTML(bytes.Buffer([]byte("\x1b[33mThis text is yellow.\x1b[m")), html)
// html: `<span style="color:olive;">This text is yellow.</span>`

html := new(bytes.Buffer)
ansihtml.ConvertToHTMLWithClasses(bytes.Buffer([]byte("\x1b[31mThis text is red.")), html, "ansi-", false)
// html: `<span class="ansi-fg-red">This text is red.</span>`
```
